from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

'''
This file contains database representations for all models
'''

app = Flask(__name__)
CORS(app)
app.debug=True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SQLALCHEMY_DATABASE_URI'] =  'mysql+pymysql://admin:gm1k6vk3@lowkeysdb-1.c0mpqsyzjjwp.us-east-1.rds.amazonaws.com:3306'


LOCAL_DB = False
if LOCAL_DB:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///lowkeys.db'    

db = SQLAlchemy(app)

class Song(db.Model):
    __tablename__ = 'songs'
    if not LOCAL_DB:
        __table_args__ = {'schema': 'songs_schema'}
    id = db.Column(db.String(255), primary_key = True)
    name = db.Column(db.String(255))
    popularity = db.Column(db.Integer)
    artists = db.Column(db.Text)
    explicit = db.Column(db.Boolean)
    duration = db.Column(db.Integer)
    genres = db.Column(db.Text)
    lyrics = db.Column(db.Text) #??
    video = db.Column(db.String(250))
    images = db.Column(db.Text)
    artists_names = db.Column(db.Text)
    
    

class Artist(db.Model):
    __tablename__ = 'artists'
    if not LOCAL_DB:
        __table_args__ = {'schema': 'artists_schema'}
    id = db.Column(db.String(255), primary_key=True)
    name=db.Column(db.String(255))
    popularity = db.Column(db.Integer)
    songs = db.Column(db.Text)
    images = db.Column(db.Text)
    followers = db.Column(db.Integer)
    genres = db.Column(db.Text)
    num_albums = db.Column(db.Integer)
    num_singles = db.Column(db.Integer)
    video = db.Column(db.Text)
    related_artists = db.Column(db.Text)
    

class Genre(db.Model):
    __tablename__ = 'genres'
    if not LOCAL_DB:
        __table_args__ = {'schema': 'genres_schema'}
    id = db.Column(db.String(255), primary_key=True)
    songs = db.Column(db.Text)
    song_num = db.Column(db.Integer)
    artist_num = db.Column(db.Integer)
    popularity = db.Column(db.Integer)
    followers = db.Column(db.Integer)
    image = db.Column(db.Text)
    video = db.Column(db.Text)
    artists = db.Column(db.Text)
    artists_names = db.Column(db.Text)


if __name__ == "__main__":
    app.run() # blocks
    