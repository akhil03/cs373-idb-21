from flask import Flask, session, request, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, and_
import requests
from models import db, app, Artist, Song, Genre
from schema import song_schema, artist_schema, genre_schema
import sys
import json



@app.route("/")
def home():
    return "LowKeys API"

@app.route("/search")
def seTemp():
    return "LowKeys API SEARCH FUNCTIONALITY"

@app.route('/artists',  methods=['GET'])
# string: list of genres, list of songs, list of related artists, 
def get_artists():
    page_num = request.args.get('page', type=int)
    num_per_page = request.args.get('per_page', type=int)

    sort_attr = request.args.get('sortBy') #sort_attr: name, popularity, singles, albums, followers
    ascend = request.args.get('asc', type=bool)

    #filterables    
    popularity_range = request.args.get('popularity')
    singles_range = request.args.get('singles')
    albums_range = request.args.get('albums')
    followers_range = request.args.get('followers')
    genre = request.args.get('genre') #filterables

    query = db.session.query(Artist)
    #total = query.count()
    result = query

    # if  page_num is not None:
    #     result = query.paginate(page=page_num, per_page=num_per_page, error_out=False)
    # else:
    #     result = query.all()

    if singles_range is not None:
        min_singles, max_singles = get_range(singles_range)
        result = result.filter(and_(Artist.num_singles >= min_singles, Artist.num_singles <= max_singles))

    if albums_range is not None:
        min_albums, max_albums = get_range(albums_range)
        result = result.filter(and_(Artist.num_albums >= min_albums, Artist.num_albums <= max_albums))
        

    if followers_range is not None:
        min_followers, max_followers = get_range(followers_range)
        result = result.filter(and_(Artist.followers >= min_followers, Artist.followers <= max_followers))


    if popularity_range is not None:
        min_popularity, max_popularity = get_range(popularity_range)
        result = result.filter(and_(Artist.popularity >= min_popularity, Artist.popularity <= max_popularity))

    if genre is not None:
        # temp = Artist.genres.split(';')
        result = result.filter(or_(Artist.genres.like(f'%;{genre};%'), Artist.genres.like(f'{genre};%')))
        # result = result.filter(temp_genre == genre for temp_genre in temp)

    # sorting
    if sort_attr is not None and getattr(Artist, sort_attr) is not None:

        if ascend is None or ascend == True:
            result = result.order_by(getattr(Artist, sort_attr).asc())
        else:
            result = result.order_by((getattr(Artist, sort_attr)).desc())
    
    total = result.count()
        
    
    if  page_num is not None:
        result = result.paginate(page=page_num, per_page=num_per_page, error_out=False) # used to be query.paginate

    

    schema_dump = artist_schema.dump(result, many=True)
    # print(schema_dump)
    for artist in schema_dump:
        # print(len(schema_dump))
        split_strings_artist(artist)   
    
    
        
    return jsonify({"artists" : schema_dump,
                    "meta": {
                        "count": total
                    }})

@app.route('/artists/<string:id>', methods=['GET'])
def get_artist_by_id(id):
    
    artist = Artist.query.filter_by(id=id)

    try:
        schema_dump = artist_schema.dump(artist, many=True)[0]
    except IndexError:
        return -1

    # print(schema_dump)
    split_strings_artist(schema_dump)
    

    return jsonify({"artist": schema_dump})



    # if artist:
    #     # return user data as JSON response
    #     return jsonify(artist)
    # else:
    #     # return error message if user not found
    #     return {'error': 'Artist not found'}

@app.route('/songs', methods=['GET'])
def get_songs():

    page_num = request.args.get('page', type=int)
    num_per_page = request.args.get('per_page', type=int)
    sort_attr = request.args.get('sortBy') #sort_attr: name, popularity, duration, artist_names, genre
    #filterables: popularity, duratiton, explicit, song name (letter), genre
    ascend = request.args.get('asc', type=bool)

    #filterables
    popularity_range = request.args.get('popularity')
    duration_range = request.args.get('duration')
    explicit = request.args.get('explicit', type=bool)
    start_char = request.args.get('start')
    genre = request.args.get('genre')

    query = db.session.query(Song)
    #total = query.count()
    result = query

    # if  page_num is not None:
    #     result = query.paginate(page=page_num, per_page=num_per_page, error_out=False).items
    # else:
    #     result = query.all()

    if start_char is not None and len(start_char) == 1:
        result = result.filter(Song.name.like(f"{start_char}%"))
    
    if genre is not None:
        result = result.filter(or_(Song.genres.like(f'%;{genre};%'), Song.genres.like(f'{genre};%')))
   
    if duration_range is not None:
        min_duration, max_duration = get_range(duration_range)
        
        result = result.filter(and_(Song.duration >= min_duration, Song.duration <= max_duration))

    if explicit is None: 
        result = result.filter(Song.explicit == False)

    if popularity_range is not None:
        min_popularity, max_popularity = get_range(popularity_range)

        result = result.filter(and_(Song.popularity >= min_popularity, Song.popularity <= max_popularity))

    # sorting
    if sort_attr is not None and getattr(Song, sort_attr) is not None:
        # print ("MADE IT")
        if ascend is None or ascend == True:
            result = result.order_by(getattr(Song, sort_attr).asc())
        else:
            result = result.order_by(getattr(Song, sort_attr).desc())
     
    total = result.count()
    
    if  page_num is not None:
        result = result.paginate(page=page_num, per_page=num_per_page, error_out=False).items # used to be query.paginate


    schema_dump = song_schema.dump(result, many=True)
    for song in schema_dump:
        split_strings_song(song)
    
    return jsonify({"songs" : schema_dump,
                    "meta": {
                        "count": total
                    }})


@app.route('/songs/<string:id>',  methods=['GET'])
def get_song_by_id(id):
    song = Song.query.filter_by(id=id)

    try:
        schema_dump = song_schema.dump(song, many=True)[0]
    except IndexError:
        return -1
    split_strings_song(schema_dump)

    
    return jsonify({"song": schema_dump})

@app.route('/genres', methods=['GET'])
def get_genres():
    page_num = request.args.get('page', type=int)
    num_per_page = request.args.get('per_page', type=int)

    '''
    id = db.Column(db.String(255), primary_key=True)
    songs = db.Column(db.Text)
    image = db.Column(db.Text)
    video = db.Column(db.Text)
    artists 
    '''
    #filterables
    song_range = request.args.get('songs')
    artists_range = request.args.get('artists')
    followers_range = request.args.get('followers')
    popularity_range = request.args.get('popularity')
    start_char = request.args.get('start')
    
    sort_attr = request.args.get('sortBy') #sorting attribute: id, song num, artists num, followers, popularity
    ascend = request.args.get('asc', type=bool)

    # query with filtering
    result = None
    query = db.session.query(Genre)
    #total = query.count()

    result = query
    
    # else:
    #     result = query.all()

    if start_char is not None and len(start_char) == 1:
        result = result.filter(Genre.id.like(f"{start_char}%"))
    
    if song_range is not None:
        min_songs, max_songs = get_range(song_range)
        result = result.filter(and_(Genre.song_num >= min_songs, Genre.song_num <= max_songs))

    if artists_range is not None: 
        min_artists, max_artists = get_range(artists_range)
        result = result.filter(and_(Genre.artist_num >= min_artists, Genre.artist_num <= max_artists))

    if followers_range is not None:
        min_followers, max_followers = get_range(followers_range)
        result = result.filter(and_(Genre.followers >= min_followers, Genre.followers <= max_followers))

    if popularity_range is not None:
        min_popularity, max_popularity = get_range(popularity_range)
        result = result.filter(and_(Genre.popularity >= min_popularity, Genre.popularity <= max_popularity))

    # searching
    
    
    #sorting
    if sort_attr is not None and getattr(Genre, sort_attr) is not None:
        # check if sort attr is valid
        print (ascend)
        if ascend is None or ascend == True:
            result = result.order_by(getattr(Genre, sort_attr).asc())
        else:
            result = result.order_by((getattr(Genre, sort_attr)).desc())

    total = result.count()
    
    #pagination
    if  page_num is not None:
        result = result.paginate(page=page_num, per_page=num_per_page, error_out=False).items # used to be query.paginate
    


    schema_dump = genre_schema.dump(result, many=True)
    for genre in schema_dump:
        
        split_strings_genre(genre)
        

    return jsonify({"genres" : schema_dump,
                    "meta": {
                        "count": total
                    }})

@app.route("/search/<string:query>", methods=['GET'])
def search_all(query):
    page_num = request.args.get('page', type=int)
    num_per_page = request.args.get('per_page', type=int)

    terms = query.split('%20') # TODO: change this to decode the URL

    search_songs_results = search_songs(terms, page_num, num_per_page)
    search_artists_results = search_artists(terms, page_num, num_per_page)
    search_genres_results = search_genres(terms, page_num, num_per_page)
    
    results = {
        **(search_songs_results[0]),
        **(search_genres_results[0]),
        **(search_artists_results[0])
    }
    
    objs = sorted(results.keys(), key=lambda x: results[x], reverse=True)
    songs = [song for song in objs if type(song) == Song]
    artists = [artist for artist in objs if type(artist) == Artist]
    genres = [genre for genre in objs if type(genre) == Genre]
    # :(
    songs_results = song_schema.dump(songs, many=True)
    artists_results = artist_schema.dump(artists, many=True)
    genres_results = genre_schema.dump(genres, many=True)

    songsCount = search_songs_results[1]
    artistsCount = search_artists_results[1]
    genresCount = search_genres_results[1]

    for song in songs_results:
        # print(songs_results)
        split_strings_song(song)
    
    for artist in artists_results:
        split_strings_artist(artist)
    
    for genre in genres_results:
        split_strings_genre(genre)
    
    return jsonify( 
         {"songs": songs_results, "artists": artists_results, "genres": genres_results, "meta": {"songsCount": songsCount, "artistsCount": artistsCount, "genresCount": genresCount}}
     )

@app.route('/genres/<string:id>', methods=['GET'])
def get_genre_by_id(id):
    genre = Genre.query.filter_by(id=id)

    try:
        schema_dump = genre_schema.dump(genre, many=True)[0]
    except IndexError:
        return -1
    
    split_strings_genre(schema_dump)

    return jsonify({"genre": schema_dump})


# @app.route('/songs/search', methods=['GET'])
def search_songs(terms, page=None, per_page=None):
    # query type: array
    results = {}
    q = []
    for term in terms:
        q.append(Song.name.contains(term))
        q.append(Song.artists_names.contains(term))
        q.append(Song.genres.contains(term))

    songs = Song.query.filter(or_(*q)) #? how does this work

    total = songs.count()

    if (page is not None):
        songs = songs.paginate(page=page, per_page=per_page, error_out=False).items
    
    for song in songs:
        if not song in results:
            results[song] = 1
        else:
            results[song] += 1

    return (results, total)  #dictionary with keys being song

# @app.route('/artists/search', methods=['GET'])
def search_artists(terms, page=None, per_page=None):
    results = {}
    total = 0
    q = []
    for term in terms:
        q.append(Artist.name.contains(term))
        q.append(Artist.genres.contains(term))
        q.append(Artist.songs.contains(term))
        #q.append(Artist.related_artists.contains(term))
    
    artists = Artist.query.filter(or_(*q))
    total = artists.count()
                
    if (page is not None):
        artists = artists.paginate(page=page, per_page=per_page, error_out=False).items

    for artist in artists:
        if not artist in results:
            results[artist] = 1
        else:
            results[artist] += 1
    
    return (results, total)

def search_genres(terms, page=None, per_page=None):
    results = {}
    q=[]

    print (page)
    print (per_page)

    for term in terms:
        q.append(Genre.id.contains(term))
        q.append(Genre.artists_names.contains(term))

        
    genres = Genre.query.filter(or_(*q))
    total = genres.count()

    if (page is not None):
        genres = genres.paginate(page=page, per_page=per_page, error_out=False).items
    # print(genres)

    for genre in genres:
        # print(genre)
        if not genre in results:
            results[genre] = 1
        else:
            results[genre] += 1 

    return (results, total)



@app.route("/search-<string:model>", methods=['GET']) #?page=1&per_page=10&q=rock
def search_models(model):
    page_num = request.args.get('page', type=int)
    num_per_page = request.args.get('per_page', type=int)
    queries = request.args.get('q', type=str) 
    if queries is not None:
        queries = queries.split('%20') # TODO: change this to decode the URL

    result = None
    res = None
    occ = None
    total = 0

    if model == 'songs':
        occ, total  = search_songs(queries, page_num, num_per_page)
        res = sorted(occ.keys(), key= lambda x: occ[x], reverse=True)
        result = song_schema.dump(res, many=True)
        for song in result:
            split_strings_song(song)


    elif model == 'artists':
        occ, total = search_artists(queries, page_num, num_per_page)
        res = sorted(occ.keys(), key= lambda x: occ[x], reverse=True)
        result = artist_schema.dump(res, many=True)
        for artist in result:
            split_strings_artist(artist)

    elif model == 'genres':
        occ, total = search_genres(queries, page_num, num_per_page)
        # print(occ)
        res = sorted(occ.keys(), key= lambda x: occ[x], reverse=True)
        result = genre_schema.dump(res, many=True)
        for genre in result:
            split_strings_genre(genre)
    
    else:
        return_err(f"Invalid model: {model}")


    return jsonify({f'{model}': result, 'meta': {'count' : total} })


def get_range(range_str):
    range_str = range_str.split('-')
    min_num = 0
    max_num = sys.maxsize

    try:
        min_num = int(range_str[0])
    except:
        pass
    
    try:
        max_num = int(range_str[1])
    except:
        pass

    return (min_num, max_num)

def return_err(text):
    resp = Response(json.dumps({"error": text}), mimetype="application/json")
    resp.error_code = 404
    return resp

def split_strings_genre(genre):
    genre['songs'] = list(filter(None,genre['songs'].split(';')))
    genre['artists'] = list(filter(None,genre['artists'].split(';')))
    genre['artists_names'] = list(filter(None,genre['artists_names'].split(';')))
    genre['image'] = list(filter(None,genre['image'].split(';')))

def split_strings_artist(artist):
    artist['related_artists'] = list(filter(None,artist['related_artists'].split(';')))
    artist['songs'] = list(filter(None,artist['songs'].split(';')))
    artist['genres'] = list(filter(None,artist['genres'].split(';')))    


def split_strings_song(song):
    song['genres'] = list(filter(None,song['genres'].split(';')))
    song['artists'] = list(filter(None,song['artists'].split(';')))
    song['artists_names'] = list(filter(None,song['artists_names'].split(';')))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)