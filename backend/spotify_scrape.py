import requests
from spotipy.oauth2 import SpotifyClientCredentials
import json, os

from google_images_search import GoogleImagesSearch

import spotipy
import spotipy.util as util
import sys
import time
import math
import lyricsgenius
from pyyoutube import Api

google_api_key = [ #would need like 9 keys for all songs to have videos...?
                
                    "AIzaSyB6ZLtQyYtn_Oits6qyyL6PgxR7T5t_U_Y", #g_b
                    "AIzaSyBM2rSo8PM4M0-Nfb8u8JHnn3IWGsgAnqo", #1372
                    "AIzaSyAPW6dT6JG1DAxYgxnMBY-wnBpCjSw0UZw", #1373
                    "AIzaSyDl3u-ThFdEKgQg7VaNPCmWL4KwDJleXuk", #1374
                    "AIzaSyCid3bQz0xhDruwCJAhyByh_8wPFMOQTnA", #1375
                  ] 


google_api_idx = 0
yt_api = Api(api_key=google_api_key[google_api_idx])


client_secret = "119b0ec1494c4f01a54da86af247c07d"
client_id = "d350003c42ca49a0a2fe54fb0666c39b"

genius_access_token = '1P8IaXgkp4chUF2l_CD8E3s2Dtcrevdp5EY8k76wkO0fQer1IGDIUWCxHwjGYcNy'
genius = lyricsgenius.Genius(genius_access_token)

all_artists = {}
all_artist_ids = set()

all_songs = {}
all_song_ids = set()

all_genres = {}
all_genre_ids = set()

def have_enough_instances():
    print('Instance Counts: ')
    print('\tArtists: ' + str(len(all_artist_ids)))
    print('\tSongs: ' + str(len(all_song_ids)))
    print('\tGenres: ' + str(len(all_genre_ids)))
    instance_counts = [len(all_artist_ids), len(all_genre_ids), len(all_song_ids)]
    instance_counts.sort()
    return (instance_counts[0] >= 50 and instance_counts[1] >= 100 and instance_counts[2] >= 150)

sp = spotipy.Spotify(auth_manager=SpotifyClientCredentials(client_secret=client_secret, client_id=client_id))

def get_popular_artists():

    LIMIT = 20
    num_requests = 2

    for j in range(num_requests):  
        
        results = sp.search(q='artist', type='artist', offset=j*LIMIT, limit=LIMIT)
        time.sleep(0.25)

        print('Iteration: ' + str(j+1) + '/' + str(num_requests))

        for artist in results['artists']['items']:

            new_artist = add_artist(artist, None)
            if have_enough_instances(): break

            if not new_artist:
                continue

            related_artists = sp.artist_related_artists(new_artist['id'])
            time.sleep(0.25)
            

            new_artist['related_artists'] = ''
            ret = True
            for related_artist in related_artists['artists']:
                new_related_artist = add_artist(related_artist, new_artist['id'], 10)
                if new_related_artist:
                    new_artist['related_artists'] += related_artist['id'] + ';'
                if have_enough_instances(): break
            else:
                ret = False
            if ret:
                break
            
    # with open('./artists2.json', 'w+') as file:
    #     json.dump(all_artists, file, indent = 4)
    
    # with open('./songs.json', 'w+') as file:
    #     json.dump(all_songs, file, indent = 4)

    print("**check artists.json***")
    
    print('Num artist added: ' + str(len(all_artist_ids)))

    print("**check genres.json***")
    print('num genres added: '+ str(len(all_genre_ids)) )
    

    print("**check songs.json***")
    print('num songs added: '+ str(len(all_songs)) )
   

def add_artist(artist, parent_id, popularity_threshold = 30):
    if artist['popularity'] < popularity_threshold or artist['id'] in all_artist_ids: # value subject to change
        return None

    new_artist = {}
    new_artist['id'] = artist['id']
    new_artist['name'] = artist['name']

    for genre in artist['genres']:
        if genre in all_genre_ids:
            continue
        all_genre_ids.add(genre)
        new_genre = {}
        new_genre['id'] = genre
        all_genres[new_genre['id']] = new_genre
        if have_enough_instances(): return None

    new_artist['genres'] = ';'.join(artist['genres'])
    
    # get number of albums
    new_artist['num_albums'] = sp.artist_albums(new_artist['id'], album_type='album')['total']

    # get number of songs
    new_artist['num_singles'] = sp.artist_albums(new_artist['id'], album_type='single')['total']

    new_artist['popularity'] = artist['popularity']
    new_artist['image'] = ''
    if artist['images'] != []:
        new_artist['image'] = artist['images'][0]['url']
        
    new_artist['followers'] = artist['followers']['total']
    new_artist['related_artists'] = parent_id # This gets overriden by all related artist ancestors

    # add songs
    top_tracks = sp.artist_top_tracks(artist_id=artist['id'])
    new_artist['top-tracks'] = ''
    i = 0
    for track in top_tracks['tracks']:
        i += 1
        if i > 3:
            break
        new_artist['top-tracks'] += track['id'] + ';'
        new_track = {}
        new_track['id'] = track['id']
        all_song_ids.add(new_track['id'])
        new_track['name'] = track['name']
        new_track['explicit'] = track['explicit']
        new_track['artists'] = ''
        new_track['genres'] = new_artist['genres']
        
        for artist in track['artists']:
            new_track['artists'] += artist['id'] + ';'
        new_track['duration'] = track['duration_ms']
        new_track['popularity'] = track['popularity']
        new_track['lyrics'] = ''
        genius_track = None
        try:
            genius_track = genius.search_song(title=new_track['name'], artist=new_artist['name']) #genius API call
        except requests.exceptions.Timeout:
            pass

        time.sleep(0.25)

        if genius_track:
            lyrics = genius_track.lyrics      
            if lyrics:
                new_track['lyrics'] = lyrics
                    
        all_songs[track['id']] = new_track
        if have_enough_instances(): break

    all_artist_ids.add(new_artist['id'])
    all_artists[new_artist['id']] = new_artist

    return new_artist

def get_yt():
    
    global yt_api
    global google_api_idx
    count = 2
    print('starting yt api')
    print("current key: "+ str(google_api_key[google_api_idx]))

    # for genre in all_genres:

    #     r = None
    #     try:
    #         r = yt_api.search_by_keywords(q='music genre ' + str(all_genres[genre]['id']), search_type='video', limit = 1, count=1).items[0]
    #     except:

    #         print('Switching API keys :p')
    #         print("old key: " + google_api_key[google_api_idx])
    #         google_api_idx += 1
    #         yt_api = Api(api_key=google_api_key[google_api_idx])
    #         print('new key: ' + google_api_key[google_api_idx])

    #         try:
    #             r = yt_api.search_by_keywords(q='music genre ' + str(all_genres[genre]['id']), search_type='video', limit = 1, count=1).items[0]
    #         except:
    #             print('Switching API keys :p')
    #             print("old key: " + google_api_key[google_api_idx])
    #             google_api_idx += 1
    #             yt_api = Api(api_key=google_api_key[google_api_idx])
    #             print('new key: ' + google_api_key[google_api_idx])
            
    #     try:
    #         all_genres[genre]['video'] = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
    #     except:
    #         print('Switching API keys :p')
    #         print("old key: " + google_api_key[google_api_idx])
    #         google_api_idx += 1
    #         yt_api = Api(api_key=google_api_key[google_api_idx])
    #         print('new key: ' + google_api_key[google_api_idx])
            
    #         all_genres[genre]['video'] = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
            


    #     print('get genre yt iteration: ' + str(count))
    #     time.sleep(0.25)
    #     count +=2

    # print ("videos for genres done")

    # for artist in all_artists:
    #     # print('YOUTUBE API: iterating through all artists')

    #     r = None
    #     try:
    #         r = yt_api.search_by_keywords(q=all_artists[artist]['name'], search_type='video',limit = 1, count=1).items[0]
    #     except:
    #         print('Switching API keys :p')

    #         print("old key: " + google_api_key[google_api_idx])
    #         google_api_idx += 1
    #         yt_api = Api(api_key=google_api_key[google_api_idx])
    #         print('new key: ' + google_api_key[google_api_idx])
    #         try:
    #             r = yt_api.search_by_keywords(q=all_artists[artist]['name'], search_type='video',limit = 1, count=1).items[0]
    #         except:
    #             print('Failed Sadge')

            
    #     try: 
    #         all_artists[artist]['video'] = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
    #     except: 
    #         print('Switching API keys :p')
    #         print("old key: " + google_api_key[google_api_idx])
    #         google_api_idx += 1
    #         yt_api = Api(api_key=google_api_key[google_api_idx])
    #         print('new key: ' + google_api_key[google_api_idx])
    #         all_artists[artist]['video'] = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml

    #     print('get artist yt iteration: ' + str(count))
    #     time.sleep(0.25)
    #     count+=2
        
    
    # print ("videos for artists done")
    # with open('./artists.json', 'w+') as file:
    #     json.dump(all_artists, file, indent = 4)

    for song in all_songs:
        keyword = all_songs[song]['artists'].split(';')[0] + ' ' + all_songs[song]['name'] #fixed syntax

        r = None
        try:
            r = yt_api.search_by_keywords(q=keyword, search_type='video', count=1, limit=1).items[0]
        except:
            print('Switching API keys :p')
            print("old key: " + google_api_key[google_api_idx])
            
            google_api_idx += 1
            if google_api_idx > 4: break
            yt_api = Api(api_key=google_api_key[google_api_idx])
            print('new key: ' + google_api_key[google_api_idx])
            try:
                r = yt_api.search_by_keywords(q=keyword, search_type='video', count=1, limit=1).items[0]
            except:
                print('Failed Sadge')
                continue

        try:
            
            all_songs[song]['video'] = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
        except:
            print('Switching API keys :p')
            print("old key: " + google_api_key[google_api_idx])
            
            google_api_idx += 1
            if google_api_idx > 4: break
            yt_api = Api(api_key=google_api_key[google_api_idx])
            print('new key: ' + google_api_key[google_api_idx])
            try:
                all_songs[song]['video'] = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
            except: 
                print('no more keys we done')
                break


        print('get song yt iteration: ' + str(count))
        count +=2
        time.sleep(0.25)

    
    print("videos for songs done")
    # with open('./songs.json', 'w+') as file:
    #     json.dump(all_songs, file, indent = 4)
    print('total yt api calls: ' + str(count))

    # except:
    #     print("YouTube API Failed")

def get_images():
    try:
        for genre in all_genres:
            print('IMAGES API: iterating through all genres')

            all_genres[genre]['images'] = get_images_from_search('music genre ' + str(all_genres[genre]['id']))
            time.sleep(0.25)
    except:
        print('Google Images API Failed')
    


'''
Google Image Search
'''
project_cx = 'e670b3c9da03249aa'
gis = GoogleImagesSearch(google_api_key, project_cx)

# define search params
# option for commonly used search param are shown below for easy reference.
# For param marked with '##':
#   - Multiselect is currently not feasible. Choose ONE option only
#   - This param can also be omitted from _search_params if you do not wish to define any value

_search_params = {
    'q': '...',
    'num': 5,
    'fileType': 'jpg|gif|png',
    'rights': 'cc_publicdomain|cc_attribute|cc_sharealike|cc_noncommercial|cc_nonderived'
}



def get_images_from_search(query, num=5):
    _search_params['q'] = query
    _search_params['num'] = num

    results = ''
    # search first, then download and resize afterwards:
    gis.search(search_params=_search_params)
    for image in gis.results():
        results += image.url + ';'  # image direct url
        #image.referrer_url  # image referrer url (source) 

    time.sleep(0.1)

    return results


def full_scrape():
    get_popular_artists()
    get_yt()
    
    
     
    # file.close()
    
    
    # file.close()

    get_images()

    # with open('./genres.json', 'w+') as file:
    #     json.dump(all_genres, file, indent = 4)
    # file.close()

    # with open('./songyoutube.json', 'w+') as file:
    #     json.dump([{song['id'] : song['video']} for song in all_songs], file, indent=4)
    # file.close()


if __name__ == '__main__':
    full_scrape()