import unittest
import requests

STAT_OK = 200

URL = "https://api.lowkeys.me"

DEBUG = False
if DEBUG == True:
    URL = "http://127.0.0.1:8000"

class Tests(unittest.TestCase):

    def setup(self):
        response = requests.get(URL)
        self.assertEqual(response.text, 'LowKeys API')

    def test0(self):
        response = requests.get(f'{URL}/songs')
        self.assertEqual(response.status_code, STAT_OK)
        res_json_count = response.json()['meta']['count']

        self.assertEqual(res_json_count, 166)

    def test1(self):
        response = requests.get(f'{URL}/songs/5WsE374LpBomAkCRSOcjWo')
        self.assertEqual(response.status_code, STAT_OK)

        song_id_found = response.json()['song']['id']
        self.assertEqual(song_id_found, '5WsE374LpBomAkCRSOcjWo')

    def test2(self):
        response = requests.get(f'{URL}/artists')
        self.assertEqual(response.status_code, STAT_OK)
        res_json_count = response.json()['meta']['count']

        self.assertEqual(res_json_count, 100)

    def test3(self):
        response = requests.get(f'{URL}/artists/6eKWqnckwdIlSnjaYgIyxv')
        self.assertEqual(response.status_code, STAT_OK)

        artist_obj_id = response.json()['artist']['id']
        self.assertEqual(artist_obj_id, '6eKWqnckwdIlSnjaYgIyxv')

    def test4(self):
        response = requests.get(f'{URL}/genres')
        self.assertEqual(response.status_code, STAT_OK)
        res_json_count = response.json()['meta']['count']

        self.assertEqual(res_json_count, 94)

    def test5(self):
        response = requests.get(f'{URL}/genres/alternative%20pop%20rock')
        self.assertEqual(response.status_code, STAT_OK)

        genre_obj_id = response.json()["genre"]["id"]
        self.assertEqual(genre_obj_id, 'alternative pop rock')
    
    def test6(self):
        response = requests.get(f'{URL}/search/the')
        self.assertEqual(response.status_code, STAT_OK)

        result = response.json()

        artists = result['artists']
        songs = result['songs']
        genres = result['genres']

        self.assertEqual(len(artists) + len(songs) + len(genres), 157)

    def test7(self):
        response = requests.get(f'{URL}/search-genres?q=pop')
        self.assertEqual(response.status_code, STAT_OK)
        self.assertEqual(len(response.json()['genres']), 14)


if __name__ == '__main__':
    unittest.main()