import time
from google_images_search import GoogleImagesSearch
import json, os


google_api_key = ['AIzaSyDl3u-ThFdEKgQg7VaNPCmWL4KwDJleXuk',
                  'AIzaSyCyTtCQT_aAuWaq2MoQielIxZZg0Ykq-GY',
                ]
project_cx = 'f26ba7f5a95274e60'


gis = GoogleImagesSearch(google_api_key[0], project_cx)



def get_images():
    genre_json = open('genres.json')
    all_genres = json.load(genre_json)
    genre_json.close()
    # try:
    for genre in all_genres:
        print('IMAGES API: iterating through all genres')

        all_genres[genre]['images'] = get_images_from_search('music genre ' + str(all_genres[genre]['id']))
        
    # except:
    #     print('Google Images API Failed')

    print(all_genres)
    with open('./genres_w_img.json', 'w+') as file:
        json.dump(all_genres, file, indent = 4)

def get_images_from_search(query, num=1):
    
    _search_params = {
        'q': query,
        'num': num,
        'fileType': 'jpg|png',
        'rights': 'cc_publicdomain'
    }
    print('search params: ')
    print(_search_params)
    results = ''
    # search first, then download and resize afterwards:
    try:
        gis.search(search_params=_search_params)
    except:
        gis = GoogleImagesSearch(google_api_key[1], project_cx)
        try:
            gis.search(search_params=_search_params)
        except:
            print("REQUEST LIMIT ERROR")
            return

    print('results: ' + str(gis.results()))

    # print('results: ' + str(gis.results()[0].url))

    time.sleep(1)
    for image in gis.results():
        results += image.url + ';'  # image direct url
        
        #image.referrer_url  # image referrer url (source) 
    # print('results:')
    # print(results)
    

    time.sleep(0.1)

    return results


if __name__ == '__main__':
    get_images()