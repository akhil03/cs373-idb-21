import requests
from spotipy.oauth2 import SpotifyClientCredentials
import json, os

import spotipy
import spotipy.util as util
import sys
import time

import math


"""
to scrape:
songs
    under # num streams
artists
    under # num monthly listeners
genres
    all?

<>

"""

client_secret = "119b0ec1494c4f01a54da86af247c07d"
client_id = "d350003c42ca49a0a2fe54fb0666c39b"

sp = spotipy.Spotify(auth_manager=SpotifyClientCredentials(client_secret=client_secret, client_id=client_id))
# token = util.prompt_for_user_token(username="lowkeysme", client_id=client_id, client_secret=client_secret, redirect_uri=None)

# get list of songs under # streams?

artists = set()
all_song_ids= set()
all_songs = {}
all_genres = {}


'''
id = db.Column(db.String(255), primary_key = True)
    name = db.Column(db.String(255))
    popularity = db.Column(db.Integer)
    artists = db.relationship('Artist', secondary=song_artist_link, backref='songs')
    album_name = db.Column(db.String(255))
    explicit = db.Column(db.Boolean)
    spotify_url = db.Column(db.String(255))
    duration = db.Column(db.Integer)
    genre = db.Column(db.String(100))
'''



#albums = sp.search(q='tag:hipster', type='album', offset=0, limit=50)
#with open('./albums.txt', 'w+') as file:
#    json.dump(albums, file, indent = 4)

# Get all hipster album_id's
# Pass album_id to spotify api call to get all songs in the album

SONGS_PER_ALBUM = 6.8 # don't change
LIMIT = 50 # don't change

def scrape_albums():
    songs_desired = 500 # approximation
    print('***Now Scraping Albums***')
    print('[Estimated wait time: ' + str(songs_desired/SONGS_PER_ALBUM/60) + ' Minutes]')

    for i in range(math.ceil(songs_desired/SONGS_PER_ALBUM/LIMIT)):
        # Get 50 albums
        results = sp.search(q='tag:hipster', type='album', offset=i*LIMIT, limit=LIMIT)

        # Get each album
        for album in results['albums']['items']:
            print('Getting Album...')
            if not album:
                continue
            scrape_songs(album['id'], album['name'], album['images'][0], album['genres'])

            # Add Artists to set
            for artist in album['artists']:
                artists.add(artist['id'])
        
        time.sleep(1)

    # Now dump results of scrape_songs() calls
    with open('./songs.json', 'w+') as file:
        json.dump(all_songs, file, indent = 4)

    print('Check songs.json!')
    print(len(artists))

def scrape_songs( album_id, album_name, album_img, genres):
    
    album_tracks = sp.album_tracks(album_id=album_id, offset=0, limit=50)
    

    for song in album_tracks['items']:
        # Don't add if song already present
        if (song['id'] in all_song_ids):
            continue
        all_song_ids.add(song['id'])

        # Add New song
        #TODO: separate track call, for audio features
        new_song = {}
        new_song['id']=song['id']
        new_song['name'] = song['name']
        new_song['duration'] = song['duration_ms']
        new_song['album_name'] = album_name
        new_song['explicit'] = song['explicit']
        new_song['image'] = album_img
        new_song['artists'] = []
        new_song['genres'] = genres
        for artist in song['artists']:
            new_song['artists'].append(artist['id'])

        all_songs[song['id']] = new_song
        
    
    time.sleep(1)

    return 

def scrape_artists():
    
    artist_data = {}
    for id in artists:
        result = sp.artist(id)

        new_artist = {}
        artist_data[id] = new_artist
        time.sleep(1)
  
    # if not os.path.exists('./artists.json'):
    with open('./artists.json', 'w+') as file:
        json.dump(artist_data, file, indent = 4)

def scrape_genres():


    return


def get_img():    
    name = 'Radiohead'

    results = sp.search(q='artist:' + name, type='artist')
    items = results['artists']['items']
    if len(items) > 0:
        artist = items[0]
        print(artist['name'], artist['images'][0]['url'])

#scrape_albums()