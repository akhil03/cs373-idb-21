import json
import time
from models import app, db, Song, Artist, Genre

'''
Script to populate the database with static json information
'''
songs_json = open('songs_w_vid.json')
songs_data = json.load(songs_json)
songs_json.close()

artists_json = open('artists.json')
artists_data = json.load(artists_json)
artists_json.close()

genres_json = open('genres_w_img.json')
genres_data = json.load(genres_json)
genres_json.close()

def populate_songs():
    #with open("songs.json") as jsn:
    #       songs_data = json.load(jsn)
    # request
    for song in songs_data:

        db_row = {

            'id': songs_data[song]['id'],
            'name': songs_data[song]['name'],
            'popularity': songs_data[song]['popularity'],
            'explicit': songs_data[song]['explicit'],
            'duration': songs_data[song]['duration'],
            'lyrics': songs_data[song]['lyrics'],
            'artists': songs_data[song]['artists'], #string with artists separated by ';'
            'artists_names' : '',
            'video': '',
            'images' : '',
            'genres': songs_data[song]['genres'] 

        }
        if 'video' in songs_data[song]:
            db_row['video'] = songs_data[song]['video']
        firstImage = True
        for artist in db_row['artists'].split(';'):
            if artist != '':
                if artist in artists_data:
                    db_row['artists_names'] += artists_data[artist]['name'] + ';'
                    if firstImage:
                        db_row['images'] = artists_data[artist]['image']
                        firstImage = False

        db.session.add(Song(**db_row))

    db.session.commit()
    #songs_json.close()
    
def populate_artists():
    #with open("artists.json") as jsn:
    #       artists_data = json.load(jsn)
    for artist in artists_data:
        db_row = {
            'id': artists_data[artist]['id'],
            'name': artists_data[artist]['name'],
            'popularity': artists_data[artist]['popularity'],
            'songs': artists_data[artist]['top-tracks'],
            'images': artists_data[artist]['image'],
            'followers': artists_data[artist]['followers'],
            'genres': artists_data[artist]['genres'],
            'num_albums': artists_data[artist]['num_albums'],
            'num_singles': artists_data[artist]['num_singles'],
            'video': artists_data[artist]['video'],
            'related_artists' : artists_data[artist]['related_artists'],
        }
        db.session.add(Artist(**db_row))
    db.session.commit()
    #songs_json.close()

def populate_genres():
    # sortable:
    # name, num artists, num songs, avg popularity?, 
    for genre in genres_data:
        db_row = {
            'id': genres_data[genre]['id'],
            'image': genres_data[genre]['images'],
            'video': genres_data[genre]['video'],
            'artists' : '',
            'songs': '',
            'popularity': 0,
            'followers': 0,
            'artists_names': ''

        }
        artist_num = 0
        artist_pop = 0
        for artist in artists_data:
            if genres_data[genre]['id'] in artists_data[artist]['genres'].split(';'):
                db_row['artists'] += artists_data[artist]['id'] + ';'
                artist_num+=1
                artist_pop+=artists_data[artist]['popularity']
                db_row['followers'] += artists_data[artist]['followers']
            
        db_row['popularity'] = artist_pop // artist_num
        db_row['artist_num'] = artist_num

        song_num = 0

        # Fill Out All Songs
        for song in songs_data:
            if genres_data[genre]['id'] in songs_data[song]['genres'].split(';'):
                db_row['songs'] += songs_data[song]['id'] + ';'
                song_num += 1
        
        db_row['song_num'] = song_num

        # Fill Out Artist Names
        for artist in db_row['artists'].split(';'):
            if artist != '':
                if artist in artists_data:
                    db_row['artists_names'] += artists_data[artist]['name'] + ';'
                
        db.session.add(Genre(**db_row))
    db.session.commit()
    #genres_json.close()

def reset():
    # db.session.remove()
    db.drop_all()
    db.create_all()


def populate():
    populate_artists()
    populate_songs()
    populate_genres()

if __name__ == "__main__":
    with app.app_context():
        reset()
        populate()




