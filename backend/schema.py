#schema for json that is returned from RESTful API
from flask_marshmallow import Marshmallow

from models import Song, Artist, Genre
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

ma = Marshmallow()

class SongSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Song

class ArtistSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Artist
        
class GenreSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Genre

song_schema = SongSchema()
artist_schema = ArtistSchema()
genre_schema = GenreSchema()