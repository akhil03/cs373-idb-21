import time

import json, os
from pyyoutube import Api


google_api_key = ['AIzaSyBbG_dpfrE9wEZVZdxu02KaMJNeCiHLCZU', #l
                  'AIzaSyBxo5zJZi1lNO0hdf508Py49CDxsN1od_o', #a
                  'AIzaSyCLNFSDIU4dyHF8PE-rhgIlm9jx9tf4KcE', #b
                  'AIzaSyDsS8YnE9crkqO_zvxbTvTXyWOv5gAtXeI', #c
                  'AIzaSyCr45JzS2biE75FtFGPrpm7YWVSTfqQrmw', #d
                ]

def get_videos():
    count = 0
    yt_api = Api(api_key=google_api_key[count])
    songs_json = open('songs.json')
    all_songs = json.load(songs_json)
    songs_json.close()

    artistjson = open('artists.json')
    all_artists = json.load(artistjson)
    artistjson.close()
    # try:
    for song in all_songs:
        try:
            keyword = all_artists[all_songs[song]['artists'].split(';')[0]]['name'] + ' ' + all_songs[song]['name'] #fixed syntax
        except:
            print('changing artists')
            keyword = 'song' + ' ' + all_songs[song]['name'] #fixed syntax

        print(keyword)
        print('IMAGES API: iterating through all songs')
        r = None
        try:
            r = yt_api.search_by_keywords(q=keyword, search_type='video', count=1, limit=1).items[0]
        except:
            print('Switching API keys :p')
            print("old key: " + google_api_key[count])
        
            count += 1
            if count > 4: break
            yt_api = Api(api_key=google_api_key[count])
            print('new key: ' + google_api_key[count])
            try:
                r = yt_api.search_by_keywords(q=keyword, search_type='video', count=1, limit=1).items[0]
            except:
                print('no more valid keys')
                break

        embed = None
        try:
            embed = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
            time.sleep(0.5)
        except:
            print('Switching API keys :p')
            print("old key: " + google_api_key[count])
            
            count += 1
            if count > 4: break
            yt_api = Api(api_key=google_api_key[count])
            print('new key: ' + google_api_key[count])
            try:
                embed = yt_api.get_video_by_id(video_id=r.id.videoId).items[0].player.embedHtml
                time.sleep(0.5)
            except: 
                print('no more keys we done')
                break

        all_songs[song]['video'] = embed
        time.sleep(.25)

        
    # except:
    #     print('Google Images API Failed')

    print(all_songs['5WsE374LpBomAkCRSOcjWo'])
    with open('./songs_w_vid.json', 'w+') as file:
        json.dump(all_songs, file, indent = 4)


if __name__ == '__main__':
    get_videos()