# api.your-voice.me/api/get/news to get news per publishers

# api.your-voice.me/api/get/senators to get terms for senators

# api.your-voice.me/api/get/committees to get D vs R committee party chair 

# https://api.your-voice.me/api/get/news?publisher=<INSERT NAME>&per_page=500

# https://api.your-voice.me/api/get/committees?chairParty=Republican

# https://api.your-voice.me/api/get/committees?chairParty=Democratic

# https://api.your-voice.me/api/get/senators?numTerms=<low>-<high>

# NEED TO ITERATE OVER THE PAGES FOR EVERY API REQUEST


import requests
import json

news_publishers_list = [
    "CNN",
    "BBC",
    "The New York Times",
    "The Washington Post",
    "Al Jazeera",
    "Reuters",
    "The Guardian",
    "Fox News",
    "NBC News",
    "ABC News",
    "CBS News",
    "Bloomberg",
    "The Wall Street Journal",
    "USA Today",
    "Los Angeles Times",
    "The Economist",
    "Time",
    "Newsweek",
    "Forbes",
    "Business Insider",
    "Vice News",
    "Politico",
    "National Geographic",
    "The Atlantic",
    "Rolling Stone",
    "HuffPost",
    "BuzzFeed News",
    "The Daily Beast",
    "The Intercept",
    "ProPublica",
    "The Verge",
    "Wired",
    "TechCrunch",
    "Ars Technica",
    "Engadget",
    "CNET", 
    "Yahoo Entertainment"
]


def yv_scrape():

    senators = {}
    dem_comm = 0
    rep_comm = 0
    publishers = {}

    # get publishers
    for pub in news_publishers_list:
        api_url = "https://api.your-voice.me/api/get/news?publisher="+ pub + "&per_page=500"
        response=requests.get(api_url)
        r = response.json()
        publishers[pub] = len(r['news'])
        
    # get senators
    api_url = "https://api.your-voice.me/api/get/senators?per_page=600"
    response=requests.get(api_url)
    r = response.json()

    for sen in r['news']:
        senators[sen['id']] = sen['terms']

    # get committees
    api_url = "https://api.your-voice.me/api/get/committees?chairParty=Republican&per_page=500"
    response=requests.get(api_url)
    r = response.json()
    rep_comm = len(r['news'])

    api_url = "https://api.your-voice.me/api/get/committees?chairParty=Democratic&per_page=500"
    response=requests.get(api_url)
    r = response.json()
    dem_comm = len(r['news'])

    all_data = {"publishers": publishers, "senators": senators, "rep_comm": rep_comm, "dem_comm": dem_comm}

    with open('yourvoice.json', 'w+') as file:
        json.dump(all_data, file, indent=4)


if __name__ == '__main__':
    yv_scrape()