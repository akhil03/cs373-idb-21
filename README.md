# Low Keys

## Group Info
### IDB Group 21
### Group Members

## Group Members (GitLab ID, EID):

Akhil Iyer: akhil03, aei346  
Jeffrey Onyediri: jonyediri, juo89  
Eric Yang: rezyta, ezy78  
Shiyu Xu: shiyuxu001, sx2632  
Raymond Vuong: Raymondv, rv23964  


## Project Misc.
Phase I Leader: Jeffrey Onyediri
Phase II Leader: Shiyu Xu  
Phase III Leader: Akhil Iyer 
Phase IV Leader: Raymond Voung

## GitLab Pipelines
https://gitlab.com/akhil03/cs373-idb-21/-/pipelines  

## Postman documentation
https://documenter.getpostman.com/view/25807199/2s93JrwQ6z

Website: https://www.lowkeys.me  

## Git SHA
Phase I SHA: f66ec263b22fcd8c98f19218241b0f054318a968  
Phase II SHA: 4a8893f2b59a32922f291ac818bf32b40ed4f280
Phase III SHA: 99a5f52f0cb1fd5b3cb719a3e23de1ae832f319e
Phase IV SHA: 68f1bdfe5630b285f0910c21eb0b8b99213dbe2b

## Phase I Estimated completion time for each member:
Akhil Iyer: 15  
Jeffrey Onyediri: 15  
Eric Yang: 15  
Shiyu Xu: 15  
Raymond Vuong: 15  

## Phase I Actual completion time for each member:
Akhil Iyer: 16  
Jeffrey Onyediri: 25  
Eric Yang: 15  
Shiyu Xu: 20  
Raymond Vuong: 30  

## Phase II Estimated completion time for each member:
Akhil Iyer: 25 
Jeffrey Onyediri: 25  
Eric Yang: 25 
Shiyu Xu: 25  
Raymond Vuong: 25  

## Phase II Actual completion time for each member:
Akhil Iyer: 30  
Jeffrey Onyediri: 35  
Eric Yang: 25  
Shiyu Xu: 30  
Raymond Vuong: 35  

## Phase III Estimated completion time for each member:
Akhil Iyer: 20 
Jeffrey Onyediri: 25  
Eric Yang: 20 
Shiyu Xu: 25  
Raymond Vuong: 25  

## Phase III Actual completion time for each member:
Akhil Iyer: 35  
Jeffrey Onyediri: 35  
Eric Yang: 30  
Shiyu Xu: 35  
Raymond Vuong: 40 

## Phase IV Estimated completion time for each member:
Akhil Iyer: 10
Jeffrey Onyediri: 10
Eric Yang: 10
Shiyu Xu: 10
Raymond Vuong: 10

## Phase IV Actual completion time for each member:
Akhil Iyer: 10
Jeffrey Onyediri: 8
Eric Yang: 6
Shiyu Xu: 10
Raymond Vuong: 10

## Comments
We were able to finish this project on time through regular meetings and splitting up the work.