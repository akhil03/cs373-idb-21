install:
	npm install
	npm install bootstrap
	npm install react-bootstrap
	npm install axios

uninstall:
	npm uninstall axios
	npm uninstall react-bootstrap
	npm uninstall bootstrap

frontend-docker:
	cd frontend
	docker run --publish 80:80 react

# frontend-docker-build:
# 	cd frontend
# 	docker build . --file frontend/Dockerfile --tag react
# 	docker build --tag react .

update:
	sudo docker build --tag frontend frontend/
	sudo docker build --tag backend backend/
	sudo docker compose up --detach --timeout 1

start:
	npm start

build:
	npm run build

push:
	git add .
	git commit -m "message"
	git push