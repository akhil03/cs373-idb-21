# Low Key


## Group Info
### IDB Group 21
### Group Members
Akhil Iyer, Jeffrey Onyediri, Eric Yang, Shiyu Xu, Raymond Vuong

## Project Information
**Website Name:** Low Key

**Project Proposal:** Low Key is a website that helps its users to find informaion about independent artists. Users will be encouraged to diversify their palate by finding underground artists that match their existing music taste.

## API's
- General music source: https://developer.spotify.com/documentation/web-api/
- General Information About Music: https://developer.prod.napster.com/api/v2.2
- Recommendations for similar songs: https://tastedive.com/read/api
- Independent artists: https://developers.soundcloud.com/docs/api/guide#search


## Models

### Songs
- **Estimated Instance Count:** About 100000
- **Attributes for Filtering and Sorting:** Name, genre, bpm, artist, length, number of stream. 
- **Media and Total Attributes:** Song name, artist, genre, length, language, upbeatness (bpm). Media: album/ep/song cover
- **Connection to Other Models:** songs created by independent Artists; songs that fit in various Genres

### Artists
- **Estimated Instance Count:** About 2000
- **Attributes for Filtering and Sorting:** Monthly listeners, genre, number of streams of most popular song, number of songs in discography  
- **Media and Total Attributes:** name, bio, monthly listeners, genre, location. Media: photo, album/song covers
- **Connection to Other Models:** artists are recommended based on different Genres and Songs

### Genres
- **Estimated Instance Count:** About 5000
- **Attributes for Filtering and Sorting:** Monthly listeners
- **Media and Total Attributes:** Name, description, artists, upbeatness measured by average BPM of songs in genre 
- **Connection to Other Models:** Artists and Songs fit in various genres

### Organizational Technique
We will have one page per model, with the information displayed in a grid format with cards. Each card will also have links to other instances.

### Questions
1. What are some up-and-coming artists?
2. What artists and genres would I enjoy based on what I already like?
3. What songs are similar to songs I know?





