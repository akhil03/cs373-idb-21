import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from sys import platform
from webdriver_manager.chrome import ChromeDriverManager


url = "https://www.lowkeys.me/"

def driver_init():

    
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    #chrome_options.add_argument("window-size=2560x1440")
    

    return webdriver.Chrome(options=chrome_options)


class CombinedGUITests(unittest.TestCase):
    def tearDown(self):
        self.driver.quit()
    
    def setUp(self):
        #self.driver = driver_init()
        #self.driver.get(url)
        if platform == "win32":
            PATH = "./guitests/chromedriver_win32.exe"
        elif platform == "linux":
            PATH = "./guitests/chromedriver_linux"
        elif platform == "darwin":
            PATH = "./guitests/chromedriver_darwin"
        else:
            print("Unsupported OS")
            exit(-1)
    
            
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome(executable_path=PATH, options=options)
        #self.webpage = url

    def testArtists(self):
        self.driver.get(url + 'Artists')
        self.assertEqual(self.driver.current_url, url + 'Artists')
        #print("test artist")
    
    def testGenres(self):
        self.driver.get(url + 'Genres')
        self.assertEqual(self.driver.current_url, url + 'Genres')
        #print("test genres")

    def testSongs(self):
        self.driver.get(url + 'Songs')
        self.assertEqual(self.driver.current_url, url + 'Songs')
        #print("test songs")

    def testAbout(self):
        self.driver.get(url + 'About')
        self.assertEqual(self.driver.current_url, url + 'About')
        #print("test about")

    def testNavArtists(self):
        button = self.driver.find_element(By.ID, 'nav_artists')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Artists')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    def testNavGenres(self):
        button = self.driver.find_element(By.ID, 'nav_genres')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Genres')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    def testNavSongs(self):
        button = self.driver.find_element(By.ID, 'nav_songs')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Songs')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    def testNavAbout(self):
        button = self.driver.find_element(By.ID, 'nav_about')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'About')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    '''
    def testTitle(self):
        self.driver.get(url)
        self.assertEqual(self.driver.title, "Low Keys")
        #print("test title")

    def testHome(self):
        self.driver.get(url)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Welcome to Low Keys!")
        #print("test artist")

    def testArtistsTitle(self):
        self.driver.get(url + 'Artists')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Artists")
        #print("test artist")
    
    def testGenresTitle(self):
        self.driver.get(url + 'Genres')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Genres")
        #print("test genres")

    def testSongsTitle(self):
        self.driver.get(url + 'Songs')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Songs")
        #print("test songs")

    def testAboutTitle(self):
        self.driver.get(url + 'About')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "About")
        #print("test about")
    '''

    def testSearch(self):
        self.driver.get(url)
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/')
    
    def testSearchTwo(self):
        self.driver.get(url)
        search_form = self.driver.find_element(By.CSS_SELECTOR, "form.d-flex")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[type='search']")
        search_input.send_keys("tequila")
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/tequila')



if __name__ == "__main__":
    #chromedriver_autoinstaller.install()
    unittest.main()