import os
import unittest

from sys import platform
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

url = "https://www.lowkeys.me/"

def driver_init():

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    #chrome_options.add_argument("window-size=2560x1440")

    if platform == "win32":
        PATH = "./guitests/chromedriver_win32.exe"
    elif platform == "linux":
        PATH = "./guitests/chromedriver_linux"
    elif platform == "darwin":
        PATH = "./guitests/chromedriver_darwin"
    else:
        print("Unsupported OS")
        exit(-1)
    
    #print("platform:", platform)
    #s = Service(PATH)
    s = Service(ChromeDriverManager().install())

    #return webdriver.Chrome(executable_path=PATH, options=chrome_options)

    return webdriver.Chrome(executable_path=PATH, service=s, options=chrome_options)
    #return webdriver.Chrome( service=s, options=chrome_options)


class FilterTests(unittest.TestCase):

    def tearDown(self):
        #print("teardown")
        self.driver.quit()
    
    def setUp(self):
        #print("setup")
        self.driver = driver_init()

        #self.driver.get(url)

    def testSearch(self):
        self.driver.get(url)
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/')
    
    def testSearchTwo(self):
        self.driver.get(url)
        search_form = self.driver.find_element(By.CSS_SELECTOR, "form.d-flex")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[type='search']")
        search_input.send_keys("tequila")
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/tequila')

    def testSearchThree(self):
        self.driver.get(url)
        search_form = self.driver.find_element(By.CSS_SELECTOR, "form.d-flex")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[type='search']")
        search_input.send_keys("the")
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/the')
    
    def testSearchFour(self):
        self.driver.get(url)
        search_form = self.driver.find_element(By.CSS_SELECTOR, "form.d-flex")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[type='search']")
        search_input.send_keys("keyword")
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/keyword')

    def testSearchFive(self):
        self.driver.get(url)
        search_form = self.driver.find_element(By.CSS_SELECTOR, "form.d-flex")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[type='search']")
        search_input.send_keys("five")
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/five')
    
    def testSearchSix(self):
        self.driver.get(url)
        search_form = self.driver.find_element(By.CSS_SELECTOR, "form.d-flex")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[type='search']")
        search_input.send_keys("hello")
        submit_button = self.driver.find_element(By.XPATH, "//button[text()='Search']")
        submit_button.click()
        self.assertEqual(self.driver.current_url, url + 'search/hello')

if __name__ == '__main__':
    unittest.main()
