import os
import unittest

from sys import platform
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


url = "https://www.lowkeys.me/"

def driver_init():

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    #chrome_options.add_argument("window-size=2560x1440")

    if platform == "win32":
        PATH = "./guitests/chromedriver_win32.exe"
    elif platform == "linux":
        PATH = "./guitests/chromedriver_linux"
    elif platform == "darwin":
        PATH = "./guitests/chromedriver_darwin"
    else:
        print("Unsupported OS")
        exit(-1)
    
    #print("platform:", platform)
    # return webdriver.Chrome(executable_path=PATH, options=chrome_options)
    
    #s = Service(PATH)
    s = Service(ChromeDriverManager().install())

    #return webdriver.Chrome(executable_path=PATH, options=chrome_options)
    return webdriver.Chrome(executable_path=PATH, service=s, options=chrome_options)
    #return webdriver.Chrome(service=s, options=chrome_options)



class PageTests(unittest.TestCase):

    def tearDown(self):
        #print("teardown")
        self.driver.quit()
    
    def setUp(self):
        #print("setup")
        self.driver = driver_init()
        #self.driver.get(url)

    def testArtists(self):
        self.driver.get(url + 'Artists')
        self.assertEqual(self.driver.current_url, url + 'Artists')
        #print("test artist")
    
    def testGenres(self):
        self.driver.get(url + 'Genres')
        self.assertEqual(self.driver.current_url, url + 'Genres')
        #print("test genres")

    def testSongs(self):
        self.driver.get(url + 'Songs')
        self.assertEqual(self.driver.current_url, url + 'Songs')
        #print("test songs")

    def testAbout(self):
        self.driver.get(url + 'About')
        self.assertEqual(self.driver.current_url, url + 'About')
        #print("test about")

if __name__ == '__main__':
    unittest.main()
