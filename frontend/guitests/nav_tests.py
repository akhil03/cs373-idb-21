import os
import unittest

from sys import platform
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.support import expected_conditions as EC

from webdriver_manager.chrome import ChromeDriverManager


url = "https://www.lowkeys.me/"

def driver_init():

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    #chrome_options.add_argument("window-size=2560x1440")

    if platform == "win32":
        PATH = "./guitests/chromedriver_win32.exe"
    elif platform == "linux":
        PATH = "./guitests/chromedriver_linux"
    elif platform == "darwin":
        PATH = "./guitests/chromedriver_darwin"
    else:
        print("Unsupported OS")
        exit(-1)
    
    #print("platform:", platform)
    # return webdriver.Chrome(executable_path=PATH, options=chrome_options)
    #s = Service(PATH)
    s = Service(ChromeDriverManager().install())

    #return webdriver.Chrome(executable_path=PATH, options=chrome_options)
    return webdriver.Chrome(executable_path=PATH, service=s, options=chrome_options)

    #return webdriver.Chrome(service=s, options=chrome_options)


class NavTests(unittest.TestCase):

    def tearDown(self):
        self.driver.quit()
    
    def setUp(self):
        self.driver = driver_init()
        self.driver.get(url)

    #tests are subject to change


    def testNavArtists(self):
        button = self.driver.find_element(By.ID, 'nav_artists')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Artists')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    def testNavGenres(self):
        button = self.driver.find_element(By.ID, 'nav_genres')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Genres')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    def testNavSongs(self):
        button = self.driver.find_element(By.ID, 'nav_songs')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Songs')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
    
    def testNavAbout(self):
        button = self.driver.find_element(By.ID, 'nav_about')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'About')
        #self.driver.back()
        #self.assertEqual(self.driver.current_url, url)
        

if __name__ == '__main__':
    unittest.main()
