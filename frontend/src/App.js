import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import Header from './components/Header.js';
import "./styles.css";
import Home from './pages/Home.js';
import About from './pages/About.js';
import Songs from './pages/Songs.js';
import Genres from './pages/Genres.js';
import Artists from './pages/Artists.js';
import Visualizations from './pages/Visualizations.js';

import ArtistPage from './pages/ArtistPage';
import SongPage from './pages/SongPage';
import GenrePage from './pages/GenrePage';
import SearchResults from './pages/SearchResults.js'


import { Route, Routes, Link, useLocation } from "react-router-dom"
import { AnimatePresence } from "framer-motion";
import ProviderVisualizations from './pages/ProviderVisualizations';



export default function App() {
  const location = useLocation();

  return (
    <>
      <NavBar/>
      <AnimatePresence exitBeforeEnter>
        <Routes location={location} key={location.pathname} >
          <Route index element={<Home />} />
          <Route path="About" element={<About />} />
          <Route path="Songs" element={<Songs />} />
          <Route path="Artists" element={<Artists />} />
          <Route path="Genres" element={<Genres />} />
          <Route path="Visualizations" element={<Visualizations/>} />
          <Route path="Provider" element={<ProviderVisualizations/>} />

          <Route path="Artists/:id" element={<ArtistPage/>} />
          <Route path="Genres/:id" element={<GenrePage/>} />
          <Route path="Songs/:id" element={<SongPage/>} />
          <Route path="/search/:query" element={<SearchResults />} />

          {/*<Route path="*" element={<PageNotFound />*/}
        </Routes>
      </AnimatePresence>
      {/* <footer id = 'foot'>Lowkeys.me incorporated</footer> */}
    {/*


        
        <Header />
      </header>
    </div>
    */}
    </>
  );
}
