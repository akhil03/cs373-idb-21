import NavBar from '../components/NavBar';
import Header from '../components/Header.js';
import { TransitionFade } from './page-anims.js'; 
import Background from '../static/images/home-banner.png';
import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';
import Popularity_genres_visualizations from '../components/visualizations/Popularity_genres_visualization.js';
import NumArtistsVisual from '../components/visualizations/NumberArtistsVisual.js';
import NumTermsDistribution from '../components/visualizations/NumTermsDistribution';
import PartyPieChart from '../components/visualizations/PartyPieChart';
import NewsPerPublisher from '../components/visualizations/NewsPerPublisher';
import ArtistPopularityDistribution from '../components/visualizations/ArtistPopularityGraph';

export default function Visualizations() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);

            
    return (
      <TransitionFade>
        <div className="App">
          <Header title = "Our Visualizations" subtitle = "Analyzing our data" url={Background} /> 
          <h1 className="header-one">Explore our visualizations:</h1>
          <div style={{display:'flex', flexDirection:'row', justifyContent: 'space-evenly', padding: '10px'}}>
          </div>
      
        </div>
        <div>
            <Popularity_genres_visualizations/>
            <NumArtistsVisual/>
            <ArtistPopularityDistribution />
        </div>
      </TransitionFade>
    );
  }