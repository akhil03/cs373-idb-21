import {motion} from "framer-motion"

import React from 'react'

function TransitionFade(props) {
  return (
    <motion.main
      className="main__container"
      initial={{ width: "100%", opacity: 0}}
      animate={{ width: "100%", opacity: 1}}
      exit={{ x: "0%", opacity: 0 }}
      transition={{ duration: 0.25 }}
    >
        {props.children}
    </motion.main>
  )
}

export { TransitionFade }