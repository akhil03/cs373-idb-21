import NavBar from '../components/NavBar';
import MyCard from '../components/MyCard';
import React, { useState, useEffect } from 'react';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import artists from '../static/artists.json';
import Header from '../components/Header';
import Background from '../static/images/artist-banner.png';
import { useLocation } from 'react-router-dom';
import songs from '../static/songs.json'
import './SongPage.css';
import { TransitionFade } from './page-anims.js';
import axios from 'axios';

function ms_to_min(ms) {
    let min = Math.floor((ms/1000/60) << 0);
    let sec = Math.floor((ms/1000) % 60);
    if (sec < 10) {
      sec = '0' + sec;
    }
  
    return (min + ':' + sec);
  }

function SongPage() {

    
    let url = useLocation().pathname;
    const [song, setSong] = useState({name: 'loading', bio: 'loading', songs: [{},{},{}]});
    const [genres, setGenres] = useState([{}]);

    useEffect(() => {
        const fetchPosts = async () => {
            let song_id = url.slice(6 + 1); // trim off the first 6 characters from the url -> s o n g s /

            // get artist from artist_id
            // here we would query the database for the artist with the idea
            // an artist is a dict
            let queried_song = null;
            const res = await axios.get('https://api.lowkeys.me/songs/' + song_id);
            
            queried_song = res.data.song;
            // var genreData = [];
            // for(let genre_id of queried_song.genres) {
            //     if (genre_id !== '') {
            //         let genre_data = null;
            //         try {
            //             genre_data = await axios.get('https://api.lowkeys.me/genres/' + genre_id);
            //             console.log("Genre: [" + genre_id + "] IS in the database :)")
            //         } catch(error) {
            //             if (error.response) {
                            
            //             } else if (error.request) {

            //             } else {

            //             }
            //             console.log("Genre: [" + genre_id + "] is not in the database!")
            //             console.log(error);
            //             continue;
            //         }
            //         genreData.push(genre_data.data.genres);
            //     }
            // }
            // setGenres(genreData);
            // console.log(genres);


            // Get the Artists on the song
            
            // let song_artists = [];
            // let imageUrl = '';
            // let imageGrabbed = false;
            // for(let artist_id of queried_song.artists) {
            //     if (artist_id !== '') {
            //         let artist_data = null;
            //         try {
            //             artist_data = await axios.get('https://api.lowkeys.me/artists/' + artist_id);
            //             console.log("Artist: [" + artist_id + "] IS in the database :)")
            //             if (imageGrabbed === false) {
            //                 imageUrl = artist_data.data.artist.images;
            //                 imageGrabbed = true;
            //             }
            //         } catch(error) {
            //             if (error.response) {

            //             } else if (error.request) {

            //             } else {

            //             }
            //             console.log("Artist: [" + artist_id + "] is not in the database!")
            //             console.log(error);
            //             continue;
            //         }
                    
            //         artist_data = artist_data.data.artist;
            //         //song_data.genres.join('|');
            //         song_artists.push(artist_data.name);
            //     }
            // }
            // queried_song.artists = song_artists;

            // Add the Lyrics Embed
            let song_lyrics = queried_song['lyrics'].replace(/\n/g, "<br />");

            song_lyrics = song_lyrics.substring(0, song_lyrics.length - 'Embed'.length);
            let new_string_len = song_lyrics.length;
            for (let i = song_lyrics.length - 1; i >= 0; i--) {
                if (!isNaN(parseInt(song_lyrics[i]))) {
                    console.log('Shrunk')
                    new_string_len --;
                } else {
                    break;
                }
            }
            song_lyrics = song_lyrics.substring(0, new_string_len);            
            setTimeout ( () => {document.getElementById('box').innerHTML = song_lyrics;} );

            // Add the Youtube Embed
            // setTimeout ( () => {document.getElementById('youtube_embed').innerHTML = (queried_song['youtube_embed'] ? queried_song['youtube_embed'] : 'N/A');} );
            // if (imageGrabbed === true)  {
            //     queried_song['image'] = imageUrl;
            // }
            console.log(queried_song);
            setSong(queried_song);
            
            // Add the Youtube Embed
            this.forceUpdate(); 
        };
        fetchPosts();
        
    }, []);

    return (
        <TransitionFade>
            <Header id={song['name']} title={song['name']} url={Background}/>
            <div id = 'mainBlock'>
            <h1 style={{padding: '10px'}}>General Information</h1>
                
                <div id={song['name']}>
                <div style={{textAlign: 'center'}} id="youtube_embed" dangerouslySetInnerHTML={{ __html: song.video }}></div>
                <span style={{ fontWeight: 'bold' }}>Duration:</span> {ms_to_min(song['duration'])}<br/>
                <span style={{ fontWeight: 'bold' }}>Popularity:</span> {song['popularity']} / 100<br/>
                    <h1>Artist(s)</h1>
                    <h6>{song['artists_names'] ? song[['artists_names']].flat().join(', '): 'N/A'}</h6>
        
                    
                    <img style={{marginTop: '10px', width: '250px', height: '250px'}} src={song['images']} />
                    <h1>Genre(s)</h1>
                    <h6>{song['genres'] ? song['genres'].join(', ') : 'N/A'}</h6>
                    <h1>Language Rating</h1>
                    <h6>{song['explicit'] == true ? "Explicit" : "Non-Explicit"}</h6>
                    <h1>Lyrics</h1>
                    <div id='box'>
                        
                    </div>
                    <br/>
                
    
                    
                    {/* <div id='youtube_embed'> */}

                    {/* </div> */}
                </div>
            </div>
        </TransitionFade>
        
    );

}

export default SongPage;