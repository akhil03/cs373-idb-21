import NavBar from '../components/NavBar';
import MyCard from '../components/MyCard';
import React, { useState, useEffect, useRef } from 'react';
import './Genres.css';
import Background from '../static/images/genres-banner.png';
import Header from '../components/Header';
//import GenrePage from '/GenrePage'
import genres from '../static/genres.json';
import Col from "react-bootstrap/Col";
import { Row } from 'react-bootstrap';
import { TransitionFade } from './page-anims';
import Form from "react-bootstrap/Form";
import axios from 'axios';
import Posts from '../components/GenrePagePosts.js'
import Pagination from '../components/Pagination.js'
import Button from "react-bootstrap/Button";
import FilterDropdown from '../components/FilterDropdown';
import RangeSlider from '../components/RangeSlider';


var queryRE = null;
var searchInput = null;
function Genres() {
    const [posts, setPosts] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage, setPostsPerPage] = useState(10);
    const [numInstances, setNumInstances] = useState(0);
    const [isQuery, setIsQuery] = useState(false);

    const [ascending, setAscending] = useState();
    const searchQuery = useRef("");
    const [sort, setSort] = useState("sort");
    const [popularity, setPopularity] = useState([0,100]);
    const [followers, setFollowers] = useState([0,1_000_000]);
    const [songs, setSongs] = useState([0,80]);
    const [artists, setArtists] = useState([0,30]);
    const [start, setStart] = useState('');


    const handlePopularityFilter = (value) => {
        setPopularity(value);
    };

    const handleFollowersFilter= (value) => {
        setFollowers(value);
    };
    const handleNumSongsFilter= (value) => {
        setSongs(value);
    };
    const handleNumArtistsFilter= (value) => {
        setArtists(value);
    };
    const handleStart= (value) => {
        setStart(value);
    };
    

    const handleOrderFilter = (value) => {
        setAscending(value == "Ascending");
    };

    const handleSortFilter = (value) => {
        setSort(value.toLowerCase().replace(" ", "_"));
    };
    
    
    const arrayEquals = (a, b) => {
        return (
            Array.isArray(a) &&
            Array.isArray(b) &&
            a.length === b.length &&
            a.every((val, index) => val === b[index])
        );
    }
    
    

    useEffect(() =>{
        const fetchPosts = async () => {
            if (!loaded) {
                var query = `https://api.lowkeys.me/genres?page=${currentPage}&per_page=${postsPerPage}`;
                
                if (searchQuery.current.value != "") {
                    query = `https://api.lowkeys.me/search-genres?q=${searchQuery.current.value}&page=${currentPage}&per_page=${postsPerPage}`;
                    console.log(query)
                    searchInput = searchQuery.current.value;
                    queryRE = new RegExp(
                        `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
                        "i"
                    );
                    setIsQuery(true);
                    
                } else {
                    queryRE = null;
                    if (!arrayEquals(popularity, [0, 100])) {
                      query += `&popularity=${popularity[0]}-${popularity[1]}`;
                    }
                    if (!arrayEquals(followers, [0, 1_000_000])) {
                      query += `&followers=${followers[0]}-${followers[1]}`;
                    }

                    if (!arrayEquals(songs, [0, 80])) {
                        query += `&songs=${songs[0]}-${songs[1]}`;
                      }

                    if (!arrayEquals(artists, [0, 30])) {
                        query += `&artists=${artists[0]}-${artists[1]}`;
                    }

                    if (start != '' && start != 'ALL'){
                        query += `&start=${start}`;
                    }


                    if (sort != "sort") {
                        if(sort == 'name'){
                            query += `&sortBy=id`;
                        }else{
                            query += `&sortBy=${sort}`;
                        }
                    }
                    if(ascending){
                        query += `&asc=${ascending}`;
                    } else {
                        query += `&asc=`;
                    }
                    
                }
                console.log(query)
          
                const res = await axios.get(query);
                setNumInstances(res.data.meta.count);
                console.log(res.data.genres)
                setPosts(res.data.genres);
                
                if (isQuery === false) {
                    setIsQuery(false);
                }
                setLoaded(true)
            }
        };
        fetchPosts();

    },[currentPage, loaded]);


    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    // const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

    
  //change page
    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber);
        setLoaded(false);
    }
    const nextPage = (pageNumber) => {
        setCurrentPage(currentPage + 1);
        setLoaded(false);
    }
    const prevPage = (pageNumber) => {
        setCurrentPage(currentPage - 1);
        setLoaded(false);
    }
    const maxPages = Math.ceil(numInstances / postsPerPage);

    return (
        <TransitionFade>
            <Header title = "Genres" subtitle = "Explore unique genres from independent artists and bands!" url={Background} /> 
            <Pagination id = 'pag' postsPerPage={postsPerPage} totalPosts={numInstances} paginate={paginate} currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} maxPages={maxPages}/>
            
            <Form
                onSubmit={(event) => {
                event.preventDefault();
                setLoaded(false);
                }}
                className="d-flex pb-5 justify-content-center"
            >                
                <Form.Control
                    ref={searchQuery}
                    style={{ width: "20vw" }}
                    type="search"
                    placeholder="Search Genres"
                    className="me-2"
                    aria-label="Search"
                />
                <Button variant="outline-secondary" onClick={() => setLoaded(false)}>
                    Search
                </Button>
            </Form>

            <Form>
                <Row className="mx-auto text-center w-50 my-4" id="row-sort">
                    <Col>
                        <Form.Label><strong>Sort By:</strong></Form.Label>

                        <FilterDropdown
                        title="Sort"
                        items={["Name", "Popularity", "Song Num", "Followers","Artist Num"]}
                        onChange={handleSortFilter} style ={{marginBottom: "10px", paddingBotton: "10px"}}
                        />
                    </Col>

                    <Col>
                        <Form.Label><strong>Order By:</strong></Form.Label>

                        <FilterDropdown
                        title="Order"
                        items={["Ascending", "Descending"]}
                        onChange={handleOrderFilter}
                        />
                    </Col>
                    
                </Row>

                <Row className="mx-auto text-center my-4" id="row-filter">
                    
                    <Col>
                        <Form.Label><strong>Popularity:</strong></Form.Label>
                        <RangeSlider
                            min={0}
                            max={100}
                            onChange={handlePopularityFilter} 
                        />
                    </Col>

                    <Col>
                        <Form.Label><strong>Followers:</strong></Form.Label>
                        <RangeSlider
                            min={0}
                            max={1_000_000}
                            onChange={handleFollowersFilter}
                        />
                    </Col>

                    <Col>
                        <Form.Label><strong>Number of Songs:</strong></Form.Label>
                        <RangeSlider
                            min={0}
                            max={80}
                            onChange={handleNumSongsFilter} 
                        />
                    </Col>

                    <Col>
                        <Form.Label><strong>Number of Artists:</strong></Form.Label>
                        <RangeSlider
                            min={0}
                            max={30}
                            onChange={handleNumArtistsFilter} 
                        />
                    </Col>
                
                    <Col>
                        <Form.Label><strong>Starting Letter:</strong></Form.Label>
                        <Row>
                            <Col>
                                <FilterDropdown
                                title="Select"
                                items={["ALL", "A", "B", "C", "D", "E", "F",'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']}
                                onChange={handleStart}
                                />
                            </Col>
                        </Row>
                    </Col>

                </Row>

                    
                
            </Form>

            <Form>
                <Row className="mx-auto text-center mt-4" id="row-submit">
                    <Col>
                        <Button
                        variant="outline-secondary"
                        onClick={() => setLoaded(false)}
                        >
                        Submit
                        </Button>
                    </Col>
                </Row>
            </Form>


            <div style={{display: 'flex', fontWeight: "bold", justifyContent: 'center', alignItems: 'center'}}>{numInstances} Genres on {maxPages} pages</div>
            <div id ='test' style={{width: '100%'}}>
                <Posts posts={posts} loaded={loaded} query={searchInput}/>
            </div>
        </TransitionFade>
        
    );
    
    
}

export default Genres;