import NavBar from '../components/NavBar';
import React, { useState, useEffect, useRef } from 'react';
import './Songs.css';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import member_list from '../static/members.json'
import Header from '../components/Header';
import Background from '../static/images/songs-banner.png'
import songs from '../static/songs.json';
import SongCard from '../components/SongCard.js'
import { TransitionFade } from './page-anims.js';
import Button from "react-bootstrap/Button";
import FilterDropdown from '../components/FilterDropdown';
import RangeSlider from '../components/RangeSlider';
import axios from 'axios';
import Posts from '../components/SongPagePosts.js'
import Pagination from '../components/Pagination.js'
import  genresJSON from '../static/genres.json'
import { MDBSwitch } from 'mdb-react-ui-kit';



var queryRE = null;
var searchInput = null;
function Songs() {
    const [posts, setPosts] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage, setPostsPerPage] = useState(10);
    const [numInstances, setNumInstances] = useState(0);
    const [isQuery, setIsQuery] = useState(false);


    // Filtering
    const [ascending, setAscending] = useState(false);
    const searchQuery = useRef("");
    const [sort, setSort] = useState("sort");
    const [popularity, setPopularity] = useState([0,100]);
    const [duration, setDuration] = useState([0,600]);
    const [explicit, setExplicit] = useState(false);
    const [genre, setGenre] = useState('Genre');
    const [start, setStart] = useState('');

    // filtering
    const handlePopularityFilter = (value) => {
        setPopularity(value);
    };

    const handleDurationFilter = (value) => {
        setDuration(value);
    };

    const handleExplicitFilter = (e) => {
        console.log(e.target.checked)
        setExplicit(e.target.checked);
    };

    const handleGenre = (value) => {
        console.log(value)
        setGenre(value);
    };

    const handleStart = (value) => {
        setStart(value);
    };

    const handleOrderFilter = (value) => {
        setAscending(value == "Ascending");
    };

    const handleSortFilter = (value) => {
        setSort(value.toLowerCase().replace(" ", "_"));
    };


    const arrayEquals = (a, b) => {
        return (
            Array.isArray(a) &&
            Array.isArray(b) &&
            a.length === b.length &&
            a.every((val, index) => val === b[index])
        );
    }



    

      
    useEffect(() =>{
        const fetchPosts = async () => {
            if (!loaded) {

            var query = `https://api.lowkeys.me/songs?page=${currentPage}&per_page=${postsPerPage}`; //https://api.lowkeys.me/

            if (searchQuery.current.value != "") {
                query = `https://api.lowkeys.me/search-songs?q=${searchQuery.current.value}&page=${currentPage}&per_page=${postsPerPage}`;
                searchInput = searchQuery.current.value;
                queryRE = new RegExp(
                    `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
                    "i"
                );
                setIsQuery(true);
                
             }else {
                queryRE = null;
                    if (!arrayEquals(popularity, [0, 100])) {
                      query += `&popularity=${popularity[0]}-${popularity[1]}`;
                    }
                    if (!arrayEquals(duration, [0, 600])) {
                      query += `&duration=${duration[0]*1000}-${duration[1]*1000}`;
                    }

                    if (explicit) {
                        query += `&explicit=`;
                        console.log('EXPLICIT') //add all
                    }else{
                        console.log('NOT EXPLICIT')
                    }

                    if (genre != 'Genre' && genre != 'SELECT') {
                        query += `&genre=${genre}`;
                    }

                    if (start != '' && start != 'ALL'){
                        query += `&start=${start}`;
                    }

                    if (sort != "sort") {
                        
                        query += `&sortBy=${sort}`;
                        
                    }
                    if(ascending){
                        query += `&asc=${ascending}`;
                    } else {
                        query += `&asc=`;
                    }
            }
            console.log('the query')
            console.log(query)
            const res = await axios.get(query);
            setNumInstances(res.data.meta.count);
            console.log(res.data.songs)
            setPosts(res.data.songs);
            //console.log(res.data.songs);
            
            if (isQuery === false) {
                setIsQuery(false);
            }
            setLoaded(true)
            
        }
    };
    fetchPosts();

        
        //console.log("FETCHED POSTS")
    
    },[currentPage, loaded]);


    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    //const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

    
  //change page
    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber);
        setLoaded(false);
    }
    const nextPage = (pageNumber) => {
        setCurrentPage(currentPage + 1);
        setLoaded(false);
    }
    const prevPage = (pageNumber) => {
        setCurrentPage(currentPage - 1);
        setLoaded(false);
    }
    const maxPages = Math.ceil(numInstances / postsPerPage);
    //let loaded = true;
    return (
        <TransitionFade>
            <Header title="Songs" subtitle = "Explore up and coming songs!" url={Background}/> 
            <Pagination id = 'pag' postsPerPage={postsPerPage} totalPosts={numInstances} paginate={paginate} currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} maxPages={maxPages}/>
            
            <Form
                onSubmit={(event) => {
                event.preventDefault();
                setLoaded(false);
                }}
                className="d-flex pb-5 justify-content-center"
            >
                <Form.Control
                    ref={searchQuery}
                    style={{ width: "20vw" }}
                    type="search"
                    placeholder="Search Songs"
                    className="me-2"
                    aria-label="Search"
                />
                <Button variant="outline-secondary" onClick={() => setLoaded(false)}>
                    Search
                </Button>
            </Form>


            <Form>
                <Row className="mx-auto text-center w-50 my-4" id="row-sort">
                    <Col>
                        <Form.Label><strong>Sort By:</strong></Form.Label>

                        <FilterDropdown
                        title="Sort"
                        items={["Name", "Popularity", "Song Num", "Followers","Artist Num"]}
                        onChange={handleSortFilter}
                        />
                    </Col>

                    <Col>
                    <   Form.Label><strong>Order By:</strong></Form.Label>

                        <FilterDropdown
                        title="Order"
                        items={["Ascending", "Descending"]}
                        onChange={handleOrderFilter}
                        />
                    </Col>
                    
                </Row>
                

                <Row className="mx-auto text-center my-4" id="row-filter">
                    
                    <Col>
                        <Form.Label><strong>Popularity:</strong></Form.Label>
                        <RangeSlider
                            min={0}
                            max={100}
                            onChange={handlePopularityFilter} 
                        />
                    </Col>

                    <Col>
                        <Form.Label><strong>Duration (sec):</strong></Form.Label>
                        <RangeSlider
                            min={0}
                            max={600}
                            onChange={handleDurationFilter}
                        />
                    </Col>
                    <Col>
             
                        <Form.Label><strong>Include Explicit:</strong></Form.Label>
                        <MDBSwitch  id='flexSwitchCheckChecked' style={{ transform: 'scale(2.4)' }}onChange={handleExplicitFilter}/>
                    </Col>
                    
                    

                    <Col>
                        <Form.Label><strong>Genre:</strong></Form.Label>
                        <Row>
                            <Col>
                                <FilterDropdown 
                                title="Genre" 
                                items={Object.keys(genresJSON)}
                                onChange={handleGenre}
                                > 
                                </FilterDropdown>
                            </Col>
                        </Row>
                    
                    </Col>
                
                    <Col>
                        <Form.Label><strong>Starting Letter:</strong></Form.Label>
                        <Row>
                            <Col>
                                <FilterDropdown
                                title="Select"
                                items={["ALL","A", "B", "C", "D", "E", "F",'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']}
                                onChange={handleStart}
                                />
                            </Col>
                        </Row>
                    </Col>

                </Row>

                    
                
            </Form>

            <Form>
                <Row className="mx-auto text-center mt-4" id="row-submit">
                    <Col>
                        <Button
                        variant="outline-secondary"
                        onClick={() => setLoaded(false)}
                        >
                        Submit
                        </Button>
                    </Col>
                </Row>
            </Form>

            
            <div style={{display: 'flex', fontWeight: "bold", justifyContent: 'center', alignItems: 'center'}}>{numInstances} Songs on {maxPages} pages</div>
            <div id ='test' style={{width: '100%'}}>
                <Posts posts={posts} loaded={loaded} query={searchInput}/>
            </div>

           
               
            
            
            
            
        </TransitionFade>
        
    );
}

export default Songs;