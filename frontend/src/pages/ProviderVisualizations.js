import NavBar from '../components/NavBar';
import Header from '../components/Header.js';
import { TransitionFade } from './page-anims.js'; 
import Background from '../static/images/home-banner.png';
import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';
import NumTermsDistribution from '../components/visualizations/NumTermsDistribution';
import PartyPieChart from '../components/visualizations/PartyPieChart';
import NewsPerPublisher from '../components/visualizations/NewsPerPublisher';

export default function ProviderVisualizations() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);

            
    return (
      <TransitionFade>
        
        <div className="App">
          <Header title = "Visualizations for Your Voice" subtitle = "Analyzing the data used by Your Voice" url={Background} /> 
          <h1 className="header-one">Your Voice Visualizations:</h1>
          <div style={{display:'flex', flexDirection:'row', justifyContent: 'space-evenly', padding: '10px'}}>
          </div>
      
        </div>
        <div>
            <NewsPerPublisher />
            <PartyPieChart />
            <NumTermsDistribution />
        </div>

      </TransitionFade>
    );
  }