import NavBar from '../components/NavBar';
import MyCard from '../components/MyCard';
import React, { useState, useEffect } from 'react';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Header from '../components/Header';
import Background from '../static/images/artist-banner.png';
import SongCard from '../components/SongCard.js'
import { useLocation } from 'react-router-dom';
import { TransitionFade } from './page-anims';
import './SongPage.css'
import axios from 'axios';


function ArtistPage(props) {
    const location = useLocation();
    const [loaded, setLoaded] = useState(false);
    let url = useLocation().pathname;
    const [artist, setArtist] = useState({name: 'loading', songs: [{},{},{}], artists_names: [], genres: [], images: '', video: '', followers: 0, id: 0, num_albums: 0, num_singles: 0, popularity: 0, related_artists: []});

    

    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                // console.log("I am in artists page")
                // let artist_id = url.slice(8 + 1) // trim off the first 8 characters from the url -> a r t i s t s /
                let temp = location.pathname.toLowerCase()
                let artist_id = temp.split("/artists/").at(-1); //Artists artists
                // console.log("Here above artist id")
                // console.log(artist_id)
                // get artist from artist_id
                // here we would query the database for the artist with the id
                // an artist is a dict
                let queried_artist = null;


                const res = await axios.get('https://api.lowkeys.me/artists/' + artist_id);

                queried_artist = res.data.artist;

                

                let artist_songs = [];
                for(let song_id of queried_artist.songs) {
                    if (song_id !== '') {
                        let song_data = await axios.get('https://api.lowkeys.me/songs/' + song_id);
                        song_data = song_data.data.song;

                        // Set the <artistsDecoded> field of each Song EDIT: NO LONGER NECESSARY
                        // Set the <image> field of each Song;
                        // let song_image = null;
                        // let song_video = null;
                        // let already_got_video = false;
                        // let already_got_image = false;
                        //song_data['artistsDecoded'] = [];

                        // The song's video will be set to any of its genre's video
                        // for (let genreName of song_data.genres) {
                        //     if (genreName !== "") {
                        //         try {
                        //             let genre_data = await axios.get('https://api.lowkeys.me/genres/' + genreName)
                        //             console.log("Found Genre: " + genreName);
                        //             if (!already_got_video) {
                        //                 song_video = genre_data.data.video;
                        //                 already_got_image = true;
                        //             }
                        //             break;
                        //         } catch {
                        //             console.log('No genre: ' + genreName);
                        //             continue;
                        //         }
                        //     }
                        // }

                        // The song's image will be set to any of its genre's image
                        // for (let artist_id of song_data.artists) {
                        //     if (artist_id !== '') {
                        //         try {
                        //             let artist_data = await axios.get('https://api.lowkeys.me/artists/' + artist_id)
                        //             console.log('Found Artist: ' + artist_id)
                        //             artist_data = artist_data.data.artist;
                        //             //song_data['artistsDecoded'].push(artist_data.name);
                        //             if (!already_got_image) {
                        //                 song_image = artist_data.images;
                        //                 already_got_image = true;
                        //             }
                        //             break;
                        //         } catch {
                        //             console.log('No Artist: ' + artist_id);
                        //             continue;
                        //         }
                        //     }
                        // }


                        // song_data['newVideo'] = song_video
                        // song_data['image'] = song_image;

                        artist_songs.push(song_data);
                }
            }


            queried_artist.songs = artist_songs;
            //console.log(queried_artist);
            

            setArtist(queried_artist);
            setLoaded(true);
            }
            // Add the Youtube Embed
            //setTimeout ( () => {document.getElementById('youtube_embed').innerHTML = artist['youtube_embed'];} );
            
        };
        
        // console.log("STARTING FETCH POSTS");
        fetchPosts();
    }, [loaded]);

    return (
        <TransitionFade>
            <Header id={artist['name']} title={artist['name']} url={Background}/>

            <div id="mainBlock">
                {console.log("QUERIED ARTIST")}
                {console.log(artist)}
                <h1 style={{padding: '10px'}}>General Information</h1>
                <img style={{marginTop: '10px', width: '250px', height: '250px'}} src={artist['images']} /><br/>
                <span style={{ fontWeight: 'bold' }}>Genres:</span> {[artist['genres']].join(', ')}<br/>
                <div style={{textAlign: 'center'}} id="youtube_embed" dangerouslySetInnerHTML={{ __html: artist.video }}></div>
                <span style={{ fontWeight: 'bold' }}>Number of Singles:</span> {artist['num_singles']}<br/>
                <span style={{ fontWeight: 'bold' }}>Number of Albums:</span> {artist['num_albums']}<br/>
                <span style={{ fontWeight: 'bold' }}>Artist Popularity:</span> {artist['popularity']} / 100<br/>
                <span style={{ fontWeight: 'bold' }}>Number of Followers:</span> {artist['followers']}
                <h1>Popular Songs</h1>
                <Row style={{width: '100%'}}
                    xs={1}
                    sm={2}
                    md={3}
                    xl={5}
                    className="g-4 p-4 justify-content-center"
                    >
                    {/* {console.log("PRINTING ARTIST ATTRS")}   */}
                    {/* {console.log(artist)} */}
                    {artist.songs.map((song) => {
                        // {console.log('inside artist page')}
                        // {console.log(song['artists_names'])}
                        return (
                            <Col key={song.id} className="d-flex align-self-stretch">
                                
                                <SongCard artists_names={song['artists_names']} popularity={song['popularity']  + " / 100"} image={song['images']} genres={song['genres']} duration={song['duration']} title={song['name']} link={("/Songs/" + song.id)}/>
                            </Col>
                        );
                    })} 
                </Row>
            </div>

            {/* <div id={artist['name']} class="mainBlock">
                <h1>Biography</h1>
                <p>{artist['bio']}</p>
                <h1>Popular Songs</h1>
                {artist['songs'].map((song) => {
                    return (
                        <Row className="d-flex align-self-stretch">
                            <SongCard title={song.title} producers={song.producers} writers={song.writers} link={("/Songs/" + song.id)} linkText={"Go to song"}/>
                        </Row>
                    );
                })}
            </div> */}

            {/* <div style={{textAlign: 'center'}} id="youtube_embed"></div> */}

        </TransitionFade>
        
    );

}

export default ArtistPage