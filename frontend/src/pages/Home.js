import './Home.css';
import NavBar from '../components/NavBar.js';
import Header from '../components/Header.js';
import "./styles.css";
import MyCard from '../components/HomeCard.js';
import Background from '../static/images/home-banner.png';
import Art from '../static/images/SHADOW.jpg';
import Gen from '../static/images/GENRES.jpg';
import Songs from '../static/images/RECORD.jpg';

import { motion } from "framer-motion";
import { TransitionFade } from './page-anims.js'; 



export default function Home() {
  return (
    <TransitionFade>
      <div className="App">
        <Header title = "Welcome to Low Keys!" subtitle = "Celebrating up-and-coming music" url={Background} /> 
        <h1 className="header-one">Explore more:</h1>
        <div style={{display:'flex', flexDirection:'row', justifyContent: 'space-evenly', padding: '10px'}}>
          <MyCard {...{image: Songs, title: "Songs", text: "Explore our expanding list of underground songs", link: '/Songs'}}/>

          <MyCard {...{image: Art, title: "Artists", text: "Explore our collection of underground artists", link: '/Artists'}}/>

          <MyCard {...{image: Gen, title: "Genres", text: "Explore our variety of up and coming genres", link: '/Genres'}}/>
        </div>
    
      </div>
    </TransitionFade>
  );
}