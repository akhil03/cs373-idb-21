import NavBar from '../components/NavBar';
import React, { useState, useEffect, useRef} from 'react';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import './Songs.css';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import member_list from '../static/members.json'
import Header from '../components/Header';
import Background from '../static/images/search-banner3.png'
import songs from '../static/songs.json';
import SongCard from '../components/SongCard.js'
import { useLocation } from "react-router-dom";
import MyCard from '../components/MyCard';
import ArtistCard from '../components/ArtistCard';
import { TransitionFade } from './page-anims.js';
import Button from "react-bootstrap/Button";
import Highlighter from 'react-highlight-words';

import FilterDropdown from '../components/FilterDropdown';

import axios from 'axios';
import SongPagePosts from '../components/SongPagePosts.js'
import GenrePagePosts from '../components/GenrePagePosts.js'
import ArtistsPagePosts from '../components/ArtistsPagePosts.js'
import Pagination from '../components/Pagination.js'


var queryRE = null;

function SearchResults() { //props is search query
    //console.log("I AM IN SEARCH RESULTS")
    const location = useLocation();
    const [posts, setPosts] = useState([]);
    const [loaded, setLoaded] = useState(false);
    
    const [isQuery, setIsQuery] = useState(false);
    const [key, setKey] = useState('songs'); //songs, artists, genres
    const query = location.pathname.split("/search/").at(-1);

    //Pagination for each tab

    //For songs
    const [postsPerPageSongs, setPostsPerPageSongs] = useState(10);
    const [currentPageSongs, setCurrentPageSongs] = useState(1);
    const [numInstancesSongs, setNumInstancesSongs] = useState(0);

    //For artists
    const [postsPerPageArtists, setPostsPerPageArtists] = useState(10);
    const [currentPageArtists, setCurrentPageArtists] = useState(1);
    const [numInstancesArtists, setNumInstancesArtists] = useState(0);

    //For genres
    const [postsPerPageGenres, setPostsPerPageGenres] = useState(10);
    const [currentPageGenres, setCurrentPageGenres] = useState(1);
    const [numInstancesGenres, setNumInstancesGenres] = useState(0);
    // const [currentPosts, setCurrentPosts] = useState([]);
    
    const[songPosts, setSongPosts] = useState([]);
    const[artistPosts, setArtistPosts] = useState([]);
    const[genrePosts, setGenrePosts] = useState([]);


      
    useEffect(() =>{
        const fetchPosts = async () => {
            const res = await axios.get(`https://api.lowkeys.me/search/${query}`)
            queryRE = new RegExp(
                `(?:${query.replaceAll(" ", "|")})`,
                "i"
            );
            // console.log("in search results page");
            //console.log(post);
            // console.log(res.data)
            
            setNumInstancesSongs(res.data.meta.songsCount);
            setNumInstancesArtists(res.data.meta.artistsCount);
            setNumInstancesGenres(res.data.meta.genresCount);

            setPosts(res.data);    
            setSongPosts(res.data['songs']);
            setArtistPosts(res.data['artists']);
            setGenrePosts(res.data['genres']);
            // setCurrentPosts(res.data['songs'].slice(indexOfFirstPost, indexOfLastPost));        
            setLoaded(true)
        }
        fetchPosts();
    },[loaded]);

    // useEffect(() =>{
    //     setCurrentPosts(res.data['songs'].slice(indexOfFirstPost, indexOfLastPost));  
    // },[]);
    

    //change page for songs
    const paginateSongs = (pageNumber) => {
        setCurrentPageSongs(pageNumber);
        // setCurrentPosts(res.data['songs'].slice(indexOfFirstPost, indexOfLastPost));   
        setLoaded(false);
    }
    const nextPageSongs = (pageNumber) => {
        setCurrentPageSongs(currentPageSongs + 1);
        setLoaded(false);
    }
    const prevPageSongs = (pageNumber) => {
        setCurrentPageSongs(currentPageSongs - 1);
        setLoaded(false);
    }

    //change page for artists
    const paginateArtists = (pageNumber) => {
        setCurrentPageArtists(pageNumber);
        setLoaded(false);
    }
    const nextPageArtists = (pageNumber) => {
        setCurrentPageArtists(currentPageArtists + 1);
        setLoaded(false);
    }
    const prevPageArtists = (pageNumber) => {
        setCurrentPageArtists(currentPageArtists - 1);
        setLoaded(false);
    }
    
    //change page for genres
    const paginateGenres = (pageNumber) => {
        setCurrentPageGenres(pageNumber);
        setLoaded(false);
    }
    const nextPageGenres = (pageNumber) => {
        setCurrentPageGenres(currentPageGenres + 1);
        setLoaded(false);
    }
    const prevPageGenres = (pageNumber) => {
        setCurrentPageGenres(currentPageGenres - 1);
        setLoaded(false);
    }


    
    const indexOfLastPostSongs = currentPageSongs * postsPerPageSongs;
    const indexOfFirstPostSongs = indexOfLastPostSongs - postsPerPageSongs;
    const currentPostsSongs = songPosts.slice(indexOfFirstPostSongs, indexOfLastPostSongs);

    const indexOfLastPostArtists = currentPageArtists * postsPerPageArtists;
    const indexOfFirstPostArtists = indexOfLastPostArtists - postsPerPageArtists;
    const currentPostsArtists = artistPosts.slice(indexOfFirstPostArtists, indexOfLastPostArtists);

    const indexOfLastPostGenres = currentPageGenres * postsPerPageGenres;
    const indexOfFirstPostGenres = indexOfLastPostGenres - postsPerPageGenres;
    const currentPostsGenres = genrePosts.slice(indexOfFirstPostGenres, indexOfLastPostGenres);

   
    // console.log("current posts")
    // console.log(currentPosts)

    const maxPagesSongs = Math.ceil(numInstancesSongs / postsPerPageSongs);
    const maxPagesArtists = Math.ceil(numInstancesArtists / postsPerPageArtists);
    const maxPagesGenres = Math.ceil(numInstancesGenres / postsPerPageGenres);

    return (
        <TransitionFade>

            <Header title="Search Results" url={Background}/> 
            <Tabs
                id="controlled-tab"
                defaultActiveKey={key}
                onSelect={(k) => setKey(k)}
                className="mb-3"
                >
                <Tab eventKey="songs" title="Songs">
                    <Pagination id = 'pag' postsPerPage={postsPerPageSongs} totalPosts={numInstancesSongs} paginate={paginateSongs} currentPage={currentPageSongs} nextPage={nextPageSongs} prevPage={prevPageSongs} maxPages={maxPagesSongs}/>
                    <div style={{display: 'flex', fontWeight: "bold", justifyContent: 'center', alignItems: 'center'}}>{numInstancesSongs} songs containing words from "{query}" on {maxPagesSongs} pages</div>
                    <div id ='test' style={{width: '100%'}}>
                        <SongPagePosts posts={currentPostsSongs} loaded={loaded} query={query} setLoaded={setLoaded}/>
                    </div>
                </Tab>
                <Tab eventKey="artists" title="Artists">
                    <Pagination id = 'pag' postsPerPage={postsPerPageArtists} totalPosts={numInstancesArtists} paginate={paginateArtists} currentPage={currentPageArtists} nextPage={nextPageArtists} prevPage={prevPageArtists} maxPages={maxPagesArtists}/>
                    <div style={{display: 'flex', fontWeight: "bold", justifyContent: 'center', alignItems: 'center'}}>{numInstancesArtists} artists containing words from "{query}" on {maxPagesArtists} pages</div>
                    <div id ='test' style={{width: '100%'}}>
                        <ArtistsPagePosts posts={currentPostsArtists} loaded={loaded} query={query}/>
                    </div>
                </Tab>
                <Tab eventKey="genres" title="Genres">
                    <Pagination id = 'pag' postsPerPage={postsPerPageGenres} totalPosts={numInstancesGenres} paginate={paginateGenres} currentPage={currentPageGenres} nextPage={nextPageGenres} prevPage={prevPageGenres} maxPages={maxPagesGenres}/>
                    <div style={{display: 'flex', fontWeight: "bold", justifyContent: 'center', alignItems: 'center'}}>{numInstancesGenres} genres containing words from "{query}" on {maxPagesGenres} pages</div>
                    <div id ='test' style={{width: '100%'}}>
                        <GenrePagePosts posts={currentPostsGenres} loaded={loaded} query={query}/>
                    </div>
                </Tab>
            </Tabs> 
        </TransitionFade>
        
    );
}

export default SearchResults;