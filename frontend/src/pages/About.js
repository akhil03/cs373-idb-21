import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
import AboutCard from '../components/AboutCard';
import './About.css';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import member_list from '../static/members.json'
import apis from '../static/apis.json'
import tools from '../static/tools.json'

import Header from '../components/Header';
import Background from '../static/images/about-banner.png';
import MyCard from '../components/MyCard.js';
import axios from 'axios';
import APICard from '../components/APICard';
import { TransitionFade } from './page-anims';

async function fetchCommits() {
  const res = await axios.get("https://gitlab.com/api/v4/projects/43392236/repository/commits?all=true&until=2023-05-01&per_page=200");
  return res;
}
async function fetchIssues() {
  const res = await axios.get("https://gitlab.com/api/v4/projects/43392236/issues");
  return res;
}

function updateCommits(data) {
  member_list.forEach((member) => member.commits = 0);
  var names = {};
  data = data.data;
  data.forEach((commit) => {
    const name = commit.committer_name;
    if (name in names) {
      names[name]++;
    } else {
      names[name] = 1;
    }
  });
  for (let name in names) {
    member_list.forEach((member) => {
      if (name == member.name || name == member.gitlab.slice(1))
        member.commits += names[name];
    });
  }
}

function updateIssues(data) {
  member_list.forEach((member) => member.issues = 0);

  var usernames = {};
  member_list.forEach((member) => {
    usernames[member.gitlab.slice(1)] = member;
  });
  
  data = data.data;
  data.forEach((issue) => {

    let username = "";

    if (issue.closed_by !== null) {
      //console.log("closed by");
      //console.log(issue.author);
      username = issue.closed_by.username;
    } else if (issue.author !== null) {
      //console.log("author");
      //console.log(issue.author);
      username = issue.author.username;
    }
    
    if (username in usernames) {
      usernames[username].issues++;
    }
  });
}

async function fetchGitLabInfo() {
  const commitInfo = await fetchCommits();
  updateCommits(commitInfo);
  const issueInfo = await fetchIssues();
  updateIssues(issueInfo);
}

function updateTotals() {
  let totalCommits = 0;
  let totalIssues = 0;
  let totalTests = 0;
  member_list.forEach((member) => {
    totalCommits += member.commits;
    totalIssues += member.issues;
    totalTests += member.unittests;
  });
  return {
    commits: totalCommits,
    issues: totalIssues,
    tests: totalTests
  }
}

function About() {
  let loaded = true;

  const [totals, setTotals] = useState(
    {commits: 0,
     issues: 0,
     tests: 0
    });

  useEffect(() => {
    async function update() {
      await fetchGitLabInfo();
      setTotals(updateTotals());
    }
    update();
  }, []);


  return (
      <TransitionFade>

          <Header title = "About" subtitle = "Learn about Low Keys!" url={Background} /> 

          
          <div>
              <h1 className = "aboutTitle">What is Low Keys?</h1>
              <h4 className = "aboutBody">Low Keys is the site you come to when you are looking for lesser known up and coming artists, songs, and genres from all over the world.</h4>
              <h4 className = "aboutBody">You can start searching for artists, songs, and genres by simply clicking on a category in the navigation bar.</h4>
          </div>

          <div id = "howItWorks">
              <h1 className = "aboutTitle">How does it work?</h1>
              <h4 className = "aboutBody">Low Keys uses the Spotify,Youtube, Genius, and Google APIs to     
                  search for artists, songs, and genres that are on the rise in popularity.</h4>
          </div>            

          <h1 className="d-flex justify-content-center p-4 ">Meet the Team!</h1>
          <div className="p-4">
      {loaded ? (
        <Row style={{width: '100%'}}
          xs={1}
          sm={2}
          md={3}
          xl={5}
          className="g-4 p-4 justify-content-center"
          >
          {member_list.map((member) => {
            return (
              <Col key={member.name} className="d-flex align-self-stretch">
                <AboutCard  {...member} />
              </Col>
            );
          })}
        </Row>
      ) : (
        <Row style={{width: '100%'}}>
          <Col className="d-flex justify-content-center">
            <Spinner animation="grow" />
          </Col>
        </Row>
      )}

      <div id = "repoStats">
              <h1 className = "aboutTitle">Repository Statistics </h1>
              <h5 className = "aboutBody">Total Commits: {totals.commits} </h5>
              <h5 className = "aboutBody">Total Issues: {totals.issues} </h5>
              <h5 className = "aboutBody">Total Unit Tests: {totals.tests} </h5>
              <a classname = "postman" href="https://documenter.getpostman.com/view/25807199/2s93JrwQ6z" style={{display: 'flex', justifyContent: 'center', color: 'black', fontSize: '20px' }}>OUR POSTMAN</a> 
              
              <a classname = "gitlab" href="https://gitlab.com/akhil03/cs373-idb-21/-/pipelines" style={{display: 'flex', justifyContent: 'center', color: 'black', fontSize: '20px' }}>OUR GITLAB</a> 

          </div>  

      <div id = 'APIS'>
          <h1 id = 'apiHeader'>APIs Used</h1>
          {loaded ? (
        <Row style={{width: '100%'}}
          xs={1}
          sm={2}
          md={3}
          xl={4}
          className="g-4 p-4 justify-content-center"
          >
          {apis.map((api) => {
            return (
              <Col key={api.name} className="d-flex align-self-stretch">
                <APICard  {...api} link={(api.link)}/>
              </Col>
            );
          })}
        </Row>
      ) : (
        <Row style={{width: '100%'}}>
          <Col className="d-flex justify-content-center">
            <Spinner animation="grow" />
          </Col>
        </Row>
      )}
      </div>

      <div id = 'Tools'>
          <h1 id = 'apiHeader'>Tools Used</h1>
          {loaded ? (
        <Row style={{width: '100%'}}
          xs={1}
          sm={2}
          md={3}
          xl={4}
          className="g-4 p-4 justify-content-center"
          >
          {tools.map((tool) => {
            return (
              <Col key={tool.name} className="d-flex align-self-stretch">
                <APICard  {...tool} link={(tool.link)}/>
              </Col>
            );
          })}
        </Row>
      ) : (
        <Row style={{width: '100%'}}>
          <Col className="d-flex justify-content-center">
            <Spinner animation="grow" />
          </Col>
        </Row>
      )}
      </div>


      </div>

      </TransitionFade>
  );
}

export default About;