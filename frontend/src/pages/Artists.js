import NavBar from '../components/NavBar';
import MyCard from '../components/MyCard';
import React, { useState, useEffect, useRef } from 'react';
import './Artists.css';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import artists from '../static/artists.json';
import Header from '../components/Header';
import Background from '../static/images/artist-banner.png';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { motion } from 'framer-motion';
import { TransitionFade } from './page-anims';
import Button from "react-bootstrap/Button";

import FilterDropdown from '../components/FilterDropdown';
import RangeSlider from '../components/RangeSlider';
import { Checkbox } from 'react-bootstrap';
import { MDBSwitch } from 'mdb-react-ui-kit';
import  genresJSON from '../static/genres.json'



import axios from 'axios';
import Posts from '../components/ArtistsPagePosts.js'
import Pagination from '../components/Pagination.js'
var queryRE = null;
var searchInput = null;

function Artists() {
  const [posts, setPosts] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);
  const [numInstances, setNumInstances] = useState(0);
  const [search, setSearch] = useState('');
  const [isQuery, setIsQuery] = useState(false);
  
  
  const [ascending, setAscending] = useState(false);
  const searchQuery = useRef("");
  const [sort, setSort] = useState("sort");

  const [popularity, setPopularity] = useState([0,100]);
  const [followers, setFollowers] = useState([0,10_000_000]);
  const [numSingles, setNumSingles] = useState([0,1_000]);
  const [numAlbums, setNumAlbums] = useState([0,100]);
  const [genre, setGenre] = useState('Genre');



  const handlePopularityFilter = (value) => {
    setPopularity(value);
  };
  const handleGenre = (value) => {
    console.log(value)
    setGenre(value);
};
  const handleFollowers = (value) => {
    setFollowers(value);
  };
  const handleNumSingles = (value) => {
    setNumSingles(value);
  };

  const handleNumAlbums = (value) => {
    setNumAlbums(value);
  };

  const handleOrderFilter = (value) => {
    setAscending(value == "Ascending");
  };

  const handleSortFilter = (value) => {
    setSort(value.toLowerCase().replace(" ", "_"));
  };

  const arrayEquals = (a, b) => {
    return (
        Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index])
    );
  }
  


  useEffect(() =>{
    const fetchPosts = async () => {
      console.log('running');
      if (!loaded) {
        console.log(currentPage)
        var query = `https://api.lowkeys.me/artists?page=${currentPage}&per_page=${postsPerPage}`; //https://api.lowkeys.me
        if (searchQuery.current.value != "") {
          query = `https://api.lowkeys.me/search-artists?q=${searchQuery.current.value}&page=${currentPage}&per_page=${postsPerPage}`; //https://api.lowkeys.me
          searchInput = searchQuery.current.value;
          queryRE = new RegExp(
              `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
              "i"
          )
          setIsQuery(true);
          
       }else {
        queryRE = null;
            if (!arrayEquals(popularity, [0, 100])) {
              query += `&popularity=${popularity[0]}-${popularity[1]}`;
            }
            if (!arrayEquals(followers, [0, 10_000_000])) {
              query += `&followers=${followers[0]}-${followers[1]}`;
            }

            // if (explicit) {
            //     query += `&explicit=`;
            //     console.log('EXPLICIT') //add all
            // }else{
            //     console.log('NOT EXPLICIT')
            // }
            if (!arrayEquals(numSingles, [0, 1_000])) {
              query += `&singles=${numSingles[0]}-${numSingles[1]}`;
            }

            if (!arrayEquals(numAlbums, [0, 100])) {
              query += `&albums=${numAlbums[0]}-${numAlbums[1]}`;
            }

            if (genre != 'Genre') {
              query += `&genre=${genre}`;
            }

            if (sort != "sort") {
                
                query += `&sortBy=${sort}`;
                
            }
            if(ascending){
                query += `&asc=${ascending}`;
            } else {
                query += `&asc=`;
            }
    }
        
        const res = await axios.get(query);
        // console.log(posts)
        // console.log(res.data.meta)
        setNumInstances(res.data.meta.count)
        setPosts(res.data.artists);
        // console.log(res.data.songs);
        
        if (isQuery === false) {
          setIsQuery(false);
        }
        setLoaded(true);
      
      }
  }

    fetchPosts();
  },[currentPage, loaded]);

  // async function getArtistPosts(page, end) {
  //   const res = await client.get(`artists?page=${start}&per_page=${end}`);
    
  //https://api.lowkeys.me/artists?page=1&per_page=10

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);
  


  
//change page
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    setLoaded(false);
  }
  const nextPage = (pageNumber) => {
    setCurrentPage(currentPage + 1);
    setLoaded(false);
  }
  const prevPage = (pageNumber) => {
    setCurrentPage(currentPage - 1);
    setLoaded(false);
  }
  //const maxPages = (posts.length / postsPerPage);
  const maxPages = Math.ceil(numInstances / postsPerPage); //(posts.length / postsPerPage);
    return (
      <TransitionFade>
        <Header id= 'artistIm' title = "Artists" subtitle = "Explore growing artists from around the world!" url={Background}/> 
        <Pagination id = 'pag' postsPerPage={postsPerPage} totalPosts={numInstances} paginate={paginate} currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} maxPages={maxPages}/>
        
        <Form
          onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
          }}
          className="d-flex pb-5 justify-content-center"
        >
          <Form.Control
              ref={searchQuery}
              style={{ width: "20vw" }}
              type="search"
              placeholder="Search Artists"
              className="me-2"
              aria-label="Search"
          />
          <Button variant="outline-secondary" onClick={() => setLoaded(false)}>
              Search
          </Button>
        </Form>

        <Form>
              <Row className="mx-auto text-center w-50 my-4" id="row-sort">
                  <Col>
                      <Form.Label><strong>Sort By:</strong></Form.Label>

                      <FilterDropdown
                      title="Sort"
                      items={["Name", "Popularity", "Singles", "Followers","Albums"]}
                      onChange={handleSortFilter}
                      />
                  </Col>

                  <Col>
                  <   Form.Label><strong>Order By:</strong></Form.Label>

                      <FilterDropdown
                      title="Order"
                      items={["Ascending", "Descending"]}
                      onChange={handleOrderFilter}
                      />
                  </Col>
                  
              </Row>
              

              <Row className="mx-auto text-center my-4" id="row-filter">
                  
                  <Col>
                      <Form.Label><strong>Popularity:</strong></Form.Label>
                      <RangeSlider
                          min={0}
                          max={100}
                          onChange={handlePopularityFilter} 
                      />
                  </Col>

                  <Col>
                      <Form.Label><strong>Followers:</strong></Form.Label>
                      <RangeSlider
                          min={0}
                          max={10_000_000}
                          onChange={handleFollowers}
                      />
                  </Col>

                  <Col>
                      <Form.Label><strong>Singles:</strong></Form.Label>
                      <RangeSlider
                          min={0}
                          max={1000}
                          onChange={handleNumSingles}
                      />
                  </Col>

                  <Col>
                      <Form.Label><strong>Albums:</strong></Form.Label>
                      <RangeSlider
                          min={0}
                          max={100}
                          onChange={handleNumAlbums}
                      />
                  </Col>

                  {/*
                  <Col>
                      <Form.Label><strong>Include Explicit:</strong></Form.Label>
                      <MDBSwitch  id='flexSwitchCheckChecked' style={{ transform: 'scale(2.4)' }}onChange={handleExplicitFilter}/>
                  </Col>
                  */}
                  
              
              
                  <Col>
                        <Form.Label><strong>Genre:</strong></Form.Label>
                        <Row>
                            <Col>
                                <FilterDropdown 
                                title="Genre" 
                                items={Object.keys(genresJSON)}
                                onChange={handleGenre}
                                > 
                                </FilterDropdown>
                            </Col>
                        </Row>
                    
                    </Col>

              </Row>

                  
              
            </Form>

            <Form>
                <Row className="mx-auto text-center mt-4" id="row-submit">
                    <Col>
                        <Button
                        variant="outline-secondary"
                        onClick={() => setLoaded(false)}
                        >
                        Submit
                        </Button>
                    </Col>
                </Row>
            </Form>


            
        <div style={{display: 'flex', fontWeight: "bold", justifyContent: 'center', alignItems: 'center'}}>{numInstances} Artists on {maxPages} pages</div>  
          <div id ='test' style={{width: '100%'}}>
            <Posts posts={posts} loaded={loaded} query={searchInput} />
          </div>
        </TransitionFade>
    );
}

export default Artists;