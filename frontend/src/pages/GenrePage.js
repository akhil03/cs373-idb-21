//genre descriptions taken from napster
//sample genre api request: https://api.napster.com/v2.2/genres/g.397?apikey=YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4
import NavBar from '../components/NavBar';
import MyCard from '../components/MyCard';
import React, { useState, useEffect } from 'react';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import artists from '../static/artists.json';
import Header from '../components/Header';
import Background from '../static/images/artist-banner.png';
import { useLocation } from 'react-router-dom';
import genres from '../static/genres.json';
import SongCard from '../components/SongCard.js'
import './Artists.css'
import { TransitionFade } from './page-anims';
import axios from 'axios';
import { useForceUpdate } from 'framer-motion';

function GenrePage(props) {
    let url = useLocation().pathname;
    
    const [genre, setGenre] = useState({name: 'loading', bio: 'loading', video: 'loading', songs: [{},{},{}], artists: [], songs: [], popularity: 0, followers: 0, song_image: '', image:[]});
    
    

    useEffect(() => {
        const fetchPosts = async () => {
            let genre_id = url.slice(8); // trim off the first 7 characters from the url -> g e n r e s /

            // get artist from artist_id
            // here we would query the database for the artist with the idea
            // an artist is a dict
            let queried_genre = null;
            const res = await axios.get('https://api.lowkeys.me/genres/' + genre_id);

            queried_genre = res.data.genre;
            let genre_songs = [];
            for(let song_id of queried_genre.songs) {
                if (song_id !== '') {
                    let song_data = await axios.get('https://api.lowkeys.me/songs/' + song_id);
                    song_data = song_data.data.song;

                    // Set the <artistsDecoded> field of each Song EDIT: NO LONGER NECESSARY
                    // Set the <image> field of each Song;
                    let song_image = null;
                    let already_got_image = false;
                    // let followers = 0;
                    // let popularity = 0;
                    //song_data['artistsDecoded'] = []
                    for (let artist_id of song_data.artists) {
                        if (artist_id !== '') {
                            try {
                                let artist_data = await axios.get('https://api.lowkeys.me/artists/' + artist_id)
                                console.log('Found Artist: ' + artist_id)
                                artist_data = artist_data.data.artist;
                                //song_data['artistsDecoded'].push(artist_data.name);
                                if (!already_got_image) {
                                    song_image = artist_data.images;
                                    already_got_image = true;
                                }
                                break;
                            } catch {
                                console.log('No Artist: ' + artist_id);
                                continue;
                            }
                        }
                    }
                    song_data['song_image'] = song_image;
                    // queried_genre['followers'] = followers;
                    // queried_genre['popularity'] = popularity / song_data['artists_names'].length;

                    genre_songs.push(song_data);
                }
            }
            queried_genre.songs = genre_songs;
            // let g = null;
            // for (g of genres) {
            //     if (g.id == genre_id) {
            //         queried_genre = g;
            //     }
            // }
            //setTimeout ( () => {document.getElementById('youtube_embed').innerHTML = genre.video} );
            
            setGenre(queried_genre);
            console.log(genre)
            // Add the Youtube Embed
            
        };
        fetchPosts();
        
    }, []);

    return (
        <TransitionFade>
            <Header id="genre" title={genre.id} subtitle={"See How Big " + genre.id + " Really Is"} url={Background}/>
            
            {/* artists in this sgenre */}

            <div style={{padding: '40px'}} id="mainBlock">
                <div id="genre-description">
                    <h1 style={{padding: '10px'}}>General Information</h1>
                    {/* console.log({genre.image[0]}) */}
                    <img style={{marginTop: '10px', width: '250px', height: '250px'}} src={genre.image[0]} alt={'Genre has no image'} />
                    <div style={{textAlign: 'center'}} id="youtube_embed" dangerouslySetInnerHTML={{ __html: genre.video }}></div>
                    <span style={{ fontWeight: 'bold' }}>Number of Artists:</span> {genre['artists'].length}<br/>
                    <span style={{ fontWeight: 'bold' }}>Number of Songs:</span> {genre['songs'].length}<br/>
                    <span style={{ fontWeight: 'bold' }}>Popularity:</span> {genre['popularity']} / 100<br/>
                    <span style={{ fontWeight: 'bold' }}>Followers:</span> {genre['followers'].toLocaleString("en-US")}<br/>
                    {/* {genre.desc} */}
                </div>
                <br/>
                <br/>
                <h1>Popular {genre.id} Songs</h1>
                <Row style={{width: '100%'}}
                    xs={1}
                    sm={2}
                    md={3}
                    xl={5}
                    className="g-4 p-4 justify-content-center"
                    >
                    { genre['songs'].map((song) => {
                        return (
                            <Col className="d-flex align-self-stretch">
                                <SongCard artists_names={song['artists_names']} popularity={song['popularity']  + " / 100"} image={song['song_image']} genres={song['genres'] ? song['genres'].flat().join(', ') : song['genres']} duration={song['duration']} title={song['name']} link={("/Songs/" + song.id)}/>
                            </Col>
                        );
                    }) }
                </Row>
            </div>
            
            
            {/* <div style={{textAlign: 'center'}} id="youtube_embed">
                
            </div> */}
            {/* <MyCard link="/" image="/" title="artist 1" />
            <MyCard link="/" image="/" title="artist 2" />
            <MyCard link="/" image="/" title="artist 3" /> */}
            
        </TransitionFade>
        
    )

}

export default GenrePage;