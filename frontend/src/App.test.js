import { render, screen } from '@testing-library/react';
import App from './App';
import Home from './pages/Home.js';
import About from './pages/About.js';
import Songs from './pages/Songs.js';
import Genres from './pages/Genres.js';
import Artists from './pages/Artists.js';
import NavBar from './components/NavBar';

import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';

jest.mock('./pages/Home.js');
jest.mock('./pages/About.js');
jest.mock('./pages/Songs.js');
jest.mock('./pages/Genres.js');
jest.mock('./pages/Artists.js');
//jest.mock('./components/NavBar.js');

describe("Tests for nav bar", () => {
  test("Test NavBar on the Home Page", () => {
    //NavBar.mockImplementation(() => <div>NavbarMock</div>);
    Home.mockImplementation(() => <div>HomeMock</div>);
    render (
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("Home")).toBeInTheDocument();
    expect(screen.getByText("Genres")).toBeInTheDocument();
    expect(screen.getByText("About")).toBeInTheDocument();
    expect(screen.getByText("Artists")).toBeInTheDocument();
    expect(screen.getByText("Songs")).toBeInTheDocument();
    expect(screen.getByText("HomeMock")).toBeInTheDocument();
  });

  test("Test NavBar on the About Page", () => {
    //NavBar.mockImplementation(() => <div>NavbarMock</div>);
    About.mockImplementation(() => <div>AboutMock</div>);
    render (
      <MemoryRouter initialEntries={["/About"]}>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("Home")).toBeInTheDocument();
    expect(screen.getByText("Genres")).toBeInTheDocument();
    expect(screen.getByText("About")).toBeInTheDocument();
    expect(screen.getByText("Artists")).toBeInTheDocument();
    expect(screen.getByText("Songs")).toBeInTheDocument();
    expect(screen.getByText("AboutMock")).toBeInTheDocument();
  });

  test("Test NavBar on the Songs Page", () => {
    //NavBar.mockImplementation(() => <div>NavbarMock</div>);
    Songs.mockImplementation(() => <div>SongsMock</div>);
    render (
      <MemoryRouter initialEntries={["/Songs"]}>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("Home")).toBeInTheDocument();
    expect(screen.getByText("Genres")).toBeInTheDocument();
    expect(screen.getByText("About")).toBeInTheDocument();
    expect(screen.getByText("Artists")).toBeInTheDocument();
    expect(screen.getByText("Songs")).toBeInTheDocument();
    expect(screen.getByText("SongsMock")).toBeInTheDocument();
  });

  test("Test NavBar on the Genres Page", () => {
    //NavBar.mockImplementation(() => <div>NavbarMock</div>);
    Genres.mockImplementation(() => <div>GenresMock</div>);
    render (
      <MemoryRouter initialEntries={["/Genres"]}>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("Home")).toBeInTheDocument();
    expect(screen.getByText("Genres")).toBeInTheDocument();
    expect(screen.getByText("About")).toBeInTheDocument();
    expect(screen.getByText("Artists")).toBeInTheDocument();
    expect(screen.getByText("Songs")).toBeInTheDocument();
    expect(screen.getByText("GenresMock")).toBeInTheDocument();
  });

  test("Test NavBar on the Artists Page", () => {
    //NavBar.mockImplementation(() => <div>NavbarMock</div>);
    Artists.mockImplementation(() => <div>ArtistsMock</div>);
    render (
      <MemoryRouter initialEntries={["/Artists"]}>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("Home")).toBeInTheDocument();
    expect(screen.getByText("Genres")).toBeInTheDocument();
    expect(screen.getByText("About")).toBeInTheDocument();
    expect(screen.getByText("Artists")).toBeInTheDocument();
    expect(screen.getByText("Songs")).toBeInTheDocument();
    expect(screen.getByText("ArtistsMock")).toBeInTheDocument();
  });
});

