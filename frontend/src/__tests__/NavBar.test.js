import { render, screen } from '@testing-library/react';
import renderer from "react-test-renderer";
import NavBar from "../components/NavBar";
import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';

describe("Tests for nav bar and global search bar", () => {
    test('navbar home test', () => {
        render (
        <BrowserRouter>
            <NavBar />
        </BrowserRouter>
        );
        const linkElement = screen.getByText("Home");
        expect(linkElement).toBeInTheDocument();
    });
    test('navbar Genre test', () => {
        render (
        <BrowserRouter>
            <NavBar />
        </BrowserRouter>
        );
        const linkElement = screen.getByText("Genres");
        expect(linkElement).toBeInTheDocument();
    });
    test('navbar Artists test', () => {
        render (
        <BrowserRouter>
            <NavBar />
        </BrowserRouter>
        );
        const linkElement = screen.getByText("Artists");
        expect(linkElement).toBeInTheDocument();
    });
    test('navbar songs test', () => {
        render (
        <BrowserRouter>
            <NavBar />
        </BrowserRouter>
        );
        const linkElement = screen.getByText("Songs");
        expect(linkElement).toBeInTheDocument();
    });

    test('navbar searchbar test', () => {
        render (
        <BrowserRouter>
            <NavBar />
        </BrowserRouter>
        );
        const linkElement = screen.getByText("Search");
        expect(linkElement).toBeInTheDocument();
    });

    test('navbar about test', () => {
        
        render (
        <BrowserRouter>
            <NavBar />
        </BrowserRouter>
        );
        const linkElement = screen.getByText("About");
        expect(linkElement).toBeInTheDocument();
        }
    );

    it('nav snapshot correct', () => {
        const component = renderer.create(
            <MemoryRouter>
                <NavBar />
            </MemoryRouter>
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();   
        }
    );
});