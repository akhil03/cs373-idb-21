import { render, screen } from '@testing-library/react';
import renderer from "react-test-renderer";
import SearchResults from "../pages/SearchResults";
import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';

describe("Tests for search results", () => {
    it('nav snapshot correct no query', () => {
        const component = renderer.create(
            <MemoryRouter>
                <SearchResults/>
            </MemoryRouter>
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();   
        }
    );

    it('nav snapshot correct with query', () => {
        const component = renderer.create(
            <MemoryRouter>
                <SearchResults query="the"/> 
            </MemoryRouter>
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();   
        }
    );
});