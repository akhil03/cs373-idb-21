import { render, screen } from '@testing-library/react';
import renderer from "react-test-renderer";
import SearchResults from "../pages/SearchResults";
import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';
import RangeSlider from '../components/RangeSlider';
import FilterDropdown from '../components/FilterDropdown';

describe("Tests for Sorting and Filtering", () => {
    
    
    it('RangeSlider defaults work without errors', () => {
        const component = renderer.create(
            <BrowserRouter>
            <RangeSlider />
            </BrowserRouter>

        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();   
        }
    );
    

    /*
    it('FilterDropdown defaults work without errors', () => {
       const component = renderer.create(
             <FilterDropdown />
            );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();   
        }
    );
    */
    
});