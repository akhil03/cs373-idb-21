import { render, screen } from '@testing-library/react';
import AboutCard from "../components/AboutCard";
import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';


const mockData = {
    name: "Akhil Iyer",
    gitlab: "@akhil03",
    role: "Back-end",
    bio: "I am a second-year CS major at the University of Texas at Austin. ",
    commits: 0,
    issues: 0,
    unittests: 0,
    image: "https://miro.medium.com/v2/resize:fit:1400/format:webp/1*y4XiEu279AdwRT2yiewB9Q.jpeg"
}
describe("Tests for about card", () => {
    test('about card renders test', () => {
        const card = render(<AboutCard />);
        expect(card).toBeDefined;
        
    });
    test('About card renders test', () => {
        const { getByTestId, queryByTestId } = render(<AboutCard {...mockData} />);
        expect(queryByTestId('cardTest')).toBeDefined;
    });
    test('About bio test', () => {
        const { getByTestId, queryByTestId } = render(<AboutCard {...mockData} />);
        expect(screen.getByTestId('details').closest('p')).toHaveTextContent('I am a second-year CS major at the University of Texas at Austin.');
    });
    test('About no bio test', () => {
        const { getByTestId, queryByTestId } = render(<AboutCard />);
        expect(screen.getByTestId('details').closest('p')).toHaveTextContent('');
    });
    test('About title test', () => {
        const { getByTestId, queryByTestId } = render(<AboutCard {...mockData} />);
        expect(screen.getByTestId('title').closest('div')).toHaveTextContent('Akhil Iyer');
    });
    test('About no title test', () => {
        const { getByTestId, queryByTestId } = render(<AboutCard />);
        expect(screen.getByTestId('title').closest('div')).toHaveTextContent('');
    });
});