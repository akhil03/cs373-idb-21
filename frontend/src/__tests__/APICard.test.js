import { render, screen, fireEvent } from '@testing-library/react';
import APICard from "../components/APICard";
import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';

const mockData = {
    name: "Spotify",
    bio: "Based on simple REST principles, the Spotify Web API endpoints return JSON metadata about music artists, albums, and tracks, directly from the Spotify Data Catalogue.",
    image: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Spotify_App_Logo.svg/2048px-Spotify_App_Logo.svg.png",
    link: "https://developer.spotify.com/documentation/web-api/"
}
describe("Tests for API card", () => {
    test('API card renders test', () => {
        const { getByTestId, queryByTestId } = render(<APICard {...mockData} />);
        const buttonTest = getByTestId('moreInfo');
        expect(queryByTestId('details')).toBeDefined;
        fireEvent.click(buttonTest);
        expect(screen.getByText('Learn More').closest('a')).toHaveAttribute('href', 'https://developer.spotify.com/documentation/web-api/');
    });
    test('API card button test', () => {
        const { getByTestId, queryByTestId } = render(<APICard {...mockData} />);
        const buttonTest = getByTestId('moreInfo');
        fireEvent.click(buttonTest);
        expect(screen.getByText('Learn More').closest('a')).toHaveAttribute('href', 'https://developer.spotify.com/documentation/web-api/');
    });
    test('API bio test', () => {
        const { getByTestId, queryByTestId } = render(<APICard {...mockData} />);
        expect(screen.getByTestId('details').closest('p')).toHaveTextContent('Based on simple REST principles, the Spotify Web API endpoints return JSON metadata about music artists, albums, and tracks, directly from the Spotify Data Catalogue.');
    });
    test('API no bio test', () => {
        const { getByTestId, queryByTestId } = render(<APICard />);
        expect(screen.getByTestId('details').closest('p')).toHaveTextContent('');
    });
    test('API title test', () => {
        const { getByTestId, queryByTestId } = render(<APICard {...mockData} />);
        expect(screen.getByTestId('title').closest('div')).toHaveTextContent('Spotify');
    });
    test('API no title test', () => {
        const { getByTestId, queryByTestId } = render(<APICard />);
        expect(screen.getByTestId('title').closest('div')).toHaveTextContent('');
    });
});