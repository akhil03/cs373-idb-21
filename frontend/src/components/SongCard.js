import React from 'react'

// function Card() {
//   return (
//     <div>Card</div>
//   )
// }

// export default Card;
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button';
import { useNavigate } from "react-router-dom";
import Highlighter from 'react-highlight-words';
import './AboutCard.css';

const styles = {
  cardImage: {
    objectFit: 'cover',
    border: '1px solid darkgrey',
    width: '100%',
    height: '150px',
    borderRadius: '5px'
    
  },
}

function ms_to_min(ms) {
  let min = Math.floor((ms/1000/60) << 0);
  let sec = Math.floor((ms/1000) % 60);
  if (sec < 10) {
    sec = '0' + sec;
  }

  return (min + ':' + sec);
}

export default function SongCard (props) {

  function highlightText(input) {
    if (props.query != null) {
      // console.log("Regex is below")
      let decoded_query = decodeURI(props.query);
      // console.log(decoded_query)
      // console.log(decoded_query.split(' ').flat())
      return <Highlighter 
        highlightClassName='highlight'
        //The%20Summer%20Set
        searchWords={decoded_query.split(' ')}
        autoEscape={false}
        textToHighlight={input}
      />;
    }
    return input;
  }

  //props: link, image, title, text, linkText
  const navigate = useNavigate();
  //const join_delimiter = "| ";

  //var a = [ 1, 2, 3, 4, 5, 6 ];
  //console.log(a.join('t'));
  
  //console.log(a[0]);
  

  return (
    <>
      <Card data-testid='cardTest' className='card' style={styles.card} onClick={() => {navigate(props['link'])}}>
        <Card.Body  style={{padding: '0px', width: '100%'}}>
          <img alt={props['title'] + ' Image'} data-testid='songImage' variant="top" src={props['image']} style={styles.cardImage}/>
          <div style={{ paddingTop: '5px', borderTop: '1px solid black', borderBottom: '1px solid black', width: '100%', height: '60px', backgroundColor: 'darkgray', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <Card.Title adjustsFontSizeToFit data-testid ='songTitle' style={{ padding: '10px', color: "#36783d",  textAlign: 'center'}}> {highlightText(String(props['title']))} </Card.Title>
          </div>
          <Card.Text data-testid='details' style={{padding:'10px'}}>
            {/* {console.log('inside song card')} */}

            {/* {console.log(props)} */}
            <span style={{ fontWeight: 'bold' }}>Artists: </span><span>{highlightText(String(props['artists_names']) ? [props['artists_names']].flat().join(', ') : 'N/A')} </span><br/>
            <span style={{ fontWeight: 'bold' }}>Duration:</span> {ms_to_min(props['duration'])} Minutes<br/>
            <span style={{ fontWeight: 'bold' }}>Genres:</span>  {highlightText(String(props['genres'])  ? [props['genres']].flat().join(', ') : 'N/A')} <br/>
            <span style={{ fontWeight: 'bold' }}>Popularity:</span> {props['popularity']} <br/>

          </Card.Text>

          <Card.Link style={{padding:'20px'}} href={props['link']}>{props['linkText']}</Card.Link>
        </Card.Body>
      </Card>
    </>
  );
}