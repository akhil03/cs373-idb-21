import React, { useState } from 'react';
import './Header.css';
//import Background from './background1.png';


export default function Header(props) {
  const [showBasic, setShowBasic] = useState(false);



  return (
    <header>
      <div
        className='text-center bg-image container'
        style={{position: 'relative', maxWidth: '100%', margin: '0px', padding: '0px', padding: '0%', width:"100%", height: '400px', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}
      >
        <div className='bkgimage' style={{width: '100%', blur: 'filter(5px)', height: '100%', position: 'absolute', backgroundSize: "100% 100%", backgroundImage: "url(" + props['url'] + ")"}}> </div>
        <div className='mask' style={{filter: 'blur(0px)', width: '100%', height: '100%', backgroundColor: 'rgba(0, 0, 0, 0.6)'}}>
          <div className='d-flex justify-content-center align-items-center h-100'>
            <div className='text-white'>
              <h1 style={{fontSize: '4em', fontWeight:'600'}} className='mb-3'>{props["title"]}</h1>
              <h4 className='mb-3' style={{fontSize: '2em', fontWeight:'550', opacity:'70%'}}>{props["subtitle"]}</h4>
              {/* <MDBBtn tag="a" outline size="lg">
                Call to action
              </MDBBtn> */}
            </div>
          </div>
        </div>

      </div>

    </header>
  );
}