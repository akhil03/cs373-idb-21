import React from 'react'

// function Card() {
//   return (
//     <div>Card</div>
//   )
// }

// export default Card;
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button';
import './AboutCard.css';
import { useNavigate } from "react-router-dom";

const styles = {
    cardImage: {
      objectFit: 'cover',
      border: '1px solid darkgrey',
      width: '100%',
      height: '250px',
      borderRadius: '5px'
      
    },
  }
  
export default function APICard (props) {
  return (
    <>
      <Card className='card'>
        <Card.Body style={{padding: '0px', display: 'flex', flexDirection: 'column'}}>
          <Card.Img variant="top" src={props['image']} style={styles.cardImage}/>
          
          <div style={{ paddingTop: '5px', borderTop: '1px solid black', borderBottom: '1px solid black', width: '100%', height: '30px', backgroundColor: 'darkgray', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <Card.Title data-testid = 'title' style={{ padding: '10px', color: "#36783d",  textAlign: 'center'}}> {props['name']} </Card.Title>
          </div>
          <div>
            <Card.Text data-testid = 'details' style={{padding:'20px'}}>
              {props['bio']}
            </Card.Text>
          </div>

          
        </Card.Body>
        <div style={{width: '100%', margin: '0px', padding: '5px', display: 'flex', flexDirection: 'column-reverse', bottom:'0', marginBottom:'5px'}}>
            <Button data-testid="moreInfo" href={props['link']} style={{ mt:'auto', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>Learn More</Button>
          </div>
      </Card>
    </>
  );
}