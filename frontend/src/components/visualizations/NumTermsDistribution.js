import NavBar from '../NavBar.js';
import Header from '../Header.js';
import axios from 'axios';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import React, { useState, useEffect, useRef } from 'react';
import { AreaChart, XAxis, YAxis, ReferenceArea, ReferenceDot, ReferenceLine, Brush, CartesianGrid, Area, Tooltip, ResponsiveContainer } from 'recharts';

export default function NumTermsDistribution() {

    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);

    useEffect(() => {

        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.your-voice.me/get/politicians`;
                let res = await axios.get(query);

                let data = res.data.news;
                console.log(data.length);
                console.log("END OF LENGTH")

                let term_frequencies = new Array(52).fill(0);
                data.forEach(senator => {
                    if (senator["terms"] in term_frequencies)
                        term_frequencies[senator['terms']] += 1;
                    else   
                        term_frequencies[senator['terms']] = 1;
                });

                console.log("SHOWING TERM FREQUENCIES");
                console.log(term_frequencies);
                console.log("END OF TERM FREQUENCIES");
                let sum = 0;
                for (let i = 1; i < 52; i++) {
                    sum = term_frequencies[i - 1];
                    term_frequencies[i] += sum;
                }

                let data_final = [];
                for (let i = 0; i < 52; i++) {
                    data_final.push({
                        "name": i,
                        "Number of Politicians": term_frequencies[i]
                    });
                }

                console.log(term_frequencies);
                setPosts(data_final);
                setLoaded(true);
            }
        }

        fetchPosts();
        console.log(posts);

    }, [loaded, posts]);

    return (
        
        <div fluid="md">  
            <Row style={{ width: "100%", height: 1000 }}>
                <h3 className="p-5 text-center">Distribution of Politician Terms</h3>
                <Col>
                    <ResponsiveContainer width="95%" height="80%">

                        <AreaChart width={730} height={250} data={posts}
                        margin={{ top: 10, right: 0, left: 80, bottom: 10 }}>

                            <defs>
                                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
                                </linearGradient>
                            </defs>

                            <XAxis dataKey="name" />
                            <YAxis />
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip />
                            <Area type="monotone" dataKey="Number of Politicians" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />

                        </AreaChart>

                    </ResponsiveContainer>
                </Col>
            </Row>

        </div>
    );
}