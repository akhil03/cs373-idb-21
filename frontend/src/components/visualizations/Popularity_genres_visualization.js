import NavBar from '../NavBar.js';
import Header from '../Header.js';
import axios from 'axios';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import React, { useState, useEffect, useRef } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';

export default function Popularity_genres_visualizations() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.lowkeys.me/genres`;
                const res = await axios.get(query);
                // setTempPosts(res.data.genres);
                let temp = res.data.genres;
                console.log("temp")
                console.log(temp[0]["id"])
                
                const count = res.data.meta.count;
                console.log(count)
                let tempArray = [];
                for (let i = 0; i < count; i++) {
                    let tempDict = {};
                    tempDict["Genre"] = temp[i]["id"]; 
                    tempDict["Popularity"] = temp[i]["popularity"];
                    tempArray.push(tempDict);
                }
                // console.log(tempArray)
                setPosts(tempArray);
                // setInterval(()=>console.log(posts), 0);
                setLoaded(true);
                
            }
        }
        fetchPosts();
        console.log(posts)
    }, []);



    return (
        <div fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Popularity of each Genre</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={posts}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="2 2" />
                            <XAxis dataKey="Genre" />
                            <YAxis />
                            <ReferenceLine y={50} stroke="#000" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="Popularity" fill="#8884d8" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>

        </div>
      
    );
  }