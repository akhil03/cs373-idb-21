import NavBar from '../NavBar.js';
import Header from '../Header.js';
import axios from 'axios';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import React, { useState, useEffect, useRef } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';

export default function NewsPerPublisher() {

    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.your-voice.me/get/news`;
                let res = await axios.get(query);

                let data = res.data.news;

                console.log(data);

                let pub_frequencies = {};
                data.forEach(news => {
                    if (news["publisher"] in pub_frequencies) {
                        pub_frequencies[news['publisher']] += 1;
                    } else {
                        pub_frequencies[news['publisher']] = 1;
                    }
                });
                
                let tempArray = [];
                for (const [key, value] of Object.entries(pub_frequencies)) {
                    let tempDict = {
                        "name": key,
                        "count": value
                    };

                    tempArray.push(tempDict);
                }

                setPosts(tempArray);
                setLoaded(true);

            }
        }

        fetchPosts();
        console.log(posts)
    }, [loaded, posts]);



    return (
        <div fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of News Articles per Publisher</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={posts}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="2 2" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <ReferenceLine y={50} stroke="#000" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="count" fill="#8884d8" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>

        </div>
      
    );

}