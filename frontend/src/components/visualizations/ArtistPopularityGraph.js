import NavBar from '../NavBar.js';
import Header from '../Header.js';
import axios from 'axios';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import React, { useState, useEffect, useRef } from 'react';
import { AreaChart, XAxis, YAxis, ReferenceArea, ReferenceDot, ReferenceLine, Brush, CartesianGrid, Area, Tooltip, ResponsiveContainer } from 'recharts';

export default function ArtistPopularityDistribution() {

    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);

    useEffect(() => {

        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.lowkeys.me/artists`;
                let res = await axios.get(query);

                let data = res.data.artists;

                console.log(data);

                let pop_frequencies = Array(101).fill(0);
                data.forEach(artist => {
                    if (artist["popularity"] in pop_frequencies)
                        pop_frequencies[artist["popularity"]] += 1;
                    else   
                        pop_frequencies[artist["popularity"]] = 1;
                });

                console.log("SHOWING TERM FREQUENCIES");
                console.log(pop_frequencies);
                console.log("END OF TERM FREQUENCIES");
                for (let i = 1; i < 101; i++) {
                    pop_frequencies[i] += pop_frequencies[i - 1];
                }

                let data_final = [];
                for (let i = 0; i < 101; i++) {
                    data_final.push({
                        "name": i,
                        "Number of Artists": pop_frequencies[i]
                    });
                }

                console.log(pop_frequencies);
                setPosts(data_final);
                setLoaded(true);
            }
        }

        fetchPosts();
        console.log(posts);

    }, [loaded]);

    return (
        
        <div fluid="md">  
            <Row style={{ width: "100%", height: 1500 }}>
                <h3 className="p-5 text-center">Distribution of Artist Popularities</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">

                        <AreaChart width={730} height={250} data={posts}
                        margin={{ top: 40, right: 30, left: 0, bottom: 0 }}>

                            <defs>
                                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
                                </linearGradient>
                            </defs>

                            <XAxis dataKey="name" />
                            <YAxis />
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip />
                            <Area type="monotone" dataKey="Number of Artists" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />

                        </AreaChart>

                    </ResponsiveContainer>
                </Col>
            </Row>

        </div>
    );
}