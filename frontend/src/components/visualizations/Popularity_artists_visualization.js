import NavBar from '../components/NavBar';
import Header from '../components/Header.js';
import Background from '../static/images/home-banner.png';
import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';

export default function Popularity_artists_visualizations() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.lowkeys.me/artists`;
                const res = await axios.get(query);
                let temp = res.data.genres;
                console.log("temp")
                console.log(temp[0]["id"])
                
                const count = res.data.meta.count;
                console.log(count)
                let tempArray = [];
                for (let i = 0; i < count; i++) {
                    let tempDict = {};
                    tempDict["Artist"] = temp[i]["id"]; 
                    tempDict["Popularity"] = temp[i]["popularity"];
                    tempArray.push(tempDict);
                }
                console.log(tempArray)
                setPosts(tempArray);
                setInterval(()=>console.log(posts), 0);
                setLoaded(true);
                
            }
        }
        fetchPosts();
    }, [loaded]);



    return (
      
        <div className="App">
          <Header title = "Our Visualizations" subtitle = "Analyzing our data" url={Background} /> 
          <h1 className="header-one">Explore our visualizations:</h1>
          <div style={{display:'flex', flexDirection:'row', justifyContent: 'space-evenly', padding: '10px'}}>
          </div>
      
        </div>
      
    );
  }