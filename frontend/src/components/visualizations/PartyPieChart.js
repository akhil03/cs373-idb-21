import NavBar from '../NavBar.js';
import Header from '../Header.js';
import axios from 'axios';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import React, { useState, useEffect, useRef } from 'react';
import { PieChart, Pie, Legend, Cell, Sector, Tooltip, ResponsiveContainer } from 'recharts';

export default function PartyPieChart() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.your-voice.me/get/committees?chairParty=Republican&per_page=500`;
                const repRes = await axios.get(query);

                query = `https://api.your-voice.me/get/committees?chairParty=Democratic&per_page=500`;
                const demRes = await axios.get(query);
                
                let repCount = repRes.data.news.length;
                let demCount = demRes.data.news.length;
                
                
                let tempArray = [
                    {
                        "name": "Republican",
                        "count": repCount,
                        "percent": repCount * 1.0 / (repCount + demCount)
                    },
                    {
                        "name": "Democratic",
                        "count": demCount,
                        "percent": demCount * 1.0 / (repCount + demCount)
                    }
                ];
                
                setPosts(tempArray);
                setLoaded(true);
                
            }
        }
        fetchPosts();
        console.log(posts)
    }, [loaded, posts]);

    const COLORS = ['#FF0000', '#0000FF'];
    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);
        return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    return (
        <div fluid="md">  
            <Row style={{ width: "100%", height: 1400 }}>
                <h3 className="p-5 text-center">Number of Subcommittees Chairs for each Political Party</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="count"
                                valueKey="name"
                                isAnimationActive={false}
                                data={posts}
                                cx="50%"
                                cy="50%"
                                outerRadius={500}
                                fill='#8884d8'
                                label
                            >
                                {posts.map((entry, index) => (
                                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                    ))}
                            </Pie>
                            
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>

        </div>
      
    );
}