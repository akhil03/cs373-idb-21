import axios from 'axios';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import React, { useState, useEffect, useRef } from 'react';
import { PieChart, Pie, Legend, Cell, Sector, Tooltip, ResponsiveContainer } from 'recharts';

export default function NumberArtistsVisual() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                let query = `https://api.lowkeys.me/genres`;
                const res = await axios.get(query);
                // setTempPosts(res.data.genres);
                let temp = res.data.genres;
                console.log("temp")
                console.log(temp[0]["id"])
                
                const count = res.data.meta.count;
                console.log(count)
                let tempArray = [];
                for (let i = 0; i < count; i++) {
                    let tempDict = {};
                    tempDict["name"] = temp[i]["id"]; 
                    tempDict["Number of Artists"] = temp[i]["artist_num"];
                    tempArray.push(tempDict);
                }
                // console.log(tempArray)
                setPosts(tempArray);
                // setInterval(()=>console.log(posts), 0);
                setLoaded(true);
                
            }
        }
        fetchPosts();
        console.log(posts)
    }, [loaded]);

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
    return (
        <div fluid="md">  
            <Row style={{ width: "100%", height: 1500 }}>
                <h3 className="p-5 text-center">Number of Artists in Each Genre</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={600} height={600}>
                            <Pie
                                dataKey="Number of Artists"
                                valueKey="Genre"
                                isAnimationActive={false}
                                data={posts}
                                cx="50%"
                                cy="50%"
                                outerRadius={500}
                                fill='#8884d8'
                                label
                            >
                                {posts.map((entry, index) => (
                                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                    ))}
                            </Pie>
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>

        </div>
      
    );
  }