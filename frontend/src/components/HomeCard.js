import React from 'react'

// function Card() {
//   return (
//     <div>Card</div>
//   )
// }

// export default Card;
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button';
import './AboutCard.css';
import { useNavigate } from "react-router-dom";


  
export default function MyCard (props) {
  //props: link, image, title, text, linkText

  
  const navigate = useNavigate();

  let trunc_text = props.text;
  if (props.full_text) {
  } else {
    trunc_text = props.text.replace(/(.{100})..+/, "$1…");
  }
  
  return (
    <>
      <Card className='card' onClick={() => {navigate(props['link'])}}>
        <Card.Body style={{padding: '0px'}}>
          <Card.Img variant="top" src={props['image']} style={{border: '1px solid darkgray'}}/>
          <div style={{ paddingTop: '5px', borderTop: '1px solid black', borderBottom: '1px solid black', width: '100%', height: '30px', backgroundColor: 'darkgray', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <Card.Title style={{ padding: '10px', color: "#36783d",  textAlign: 'center'}}> {props['title']} </Card.Title>
          </div>
          <Card.Text style={{padding:'20px'}}>
            {trunc_text}
          </Card.Text>

          <Card.Link style={{padding:'20px'}} href={props['link']}>{props['linkText']}</Card.Link>
        </Card.Body>
      </Card>
    </>
  );
}