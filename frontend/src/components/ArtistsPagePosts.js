import React, { useState, useEffect } from 'react';
import ArtistCard from './ArtistCard';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
const ArtistsPagePosts = ({posts, loaded, query}) => {
    // if (loading) {
    //     return <h2>Loading...</h2>
    // }
    
    return (
        <div className="list-group mb-4">  
            {loaded ? (
                <Row style={{width: '100%'}}
                    xs={1}
                    sm={2}
                    md={3}
                    xl={5}
                    className="g-4 p-4 justify-content-center"
                    >
                    
                    
                    {posts.map((post) => {
                        return (
                            <Col className="d-flex flex-grow-1 justify-content-center align-self-stretch">
                                <ArtistCard
                                    id = {post.id}
                                    image={post.images}
                                    title={post.name}
                                    genres = {[post.genres].join('. ')}
                                    link={("/Artists/" + post.id)}
                                    albums = {post.num_albums}
                                    singles = {post.num_singles}
                                    followers = {post.followers}
                                    popularity = {post.popularity}
                                    query={query}
                                />
                            </Col>
                        );
                    })}
                </Row>
                ) : (
                <Row style={{width: '100%'}}>
                    <Col className="d-flex justify-content-center">
                    <Spinner animation="grow" />
                    </Col>
                </Row>
            )}
        </div>
    );
};

export default ArtistsPagePosts