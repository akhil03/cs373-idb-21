// import { render, screen, fireEvent } from '@testing-library/react';
// import MyCard from "./MyCard";
// import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';

// const mockData = {
//     id: 0,
//     name: "mazie",
//     bio: "Pairing fatalistic lyrics with shiny alt-pop arrangements that are as infectious as they are unpredictable has seen the quick ascension of 23-year old Baltimore native mazie. Her single \"dumb dumb,\" which appears on her debut EP the rainbow cassette, is a perfect embodiment of mazie's multilayered, psychedelic twist on pop that's Stepford Wives meets Lisa Frank. It's ironic, it's iconic, and to date the track has over 35+ million cumulative streams. mazie fell in love with singing at an early age and spent most of her childhood studying classical and jazz vocals. By the time she hit her teenage years living outside of Baltimore, she was writing her own music and recording it with her neighbor, Elie Rizk (Remi Wolf, Keshi, Bella Poarch), who was teaching himself to engineer and produce in his basement studio. The two spent years experimenting and collaborating before they landed on \"no friends,\" mazie's breakout 2020 debut. Clocking in at less than two minutes, the utterly addictive single exploded online, earning widespread acclaim and quickly racked up millions of streams. \"We were in total shock,\" says mazie. \"That song really defined the project for us, and when it started going crazy, everything just accelerated.",
    
//     image: "https://www.wearetheguard.com/sites/default/files/styles/main_blog_full_image_style/public/best-new-artists-mazie.jpg?itok=yftJuk0T",
//     songs: [
//         {
//             id: 1,
//             title: "dumb dumb",
//             artist: "mazie",
//             producers: "Elie Jay Rizk, Grace Christian",
//             writers: "Elie Jay Rizk",
//             image: "https://www.wearetheguard.com/sites/default/files/styles/main_blog_full_image_style/public/best-new-artists-mazie.jpg?itok=yftJuk0T",
//             lyrics: "<div id='rg_embed_link_6984679' class='rg_embed_link' data-song-id='6984679'>Read <a href='https://genius.com/Mazie-dumb-dumb-lyrics'>“​dumb dumb” by ​mazie</a> on Genius</div> <script crossorigin src='//genius.com/songs/6984679/embed.js'></script>"
//         }
//     ],
//     youtube_embed: "<iframe width=\"1268\" height=\"713\" src=\"https://www.youtube.com/embed/_5kDQu5tUPw\" title=\"mazie - it's not me (it's u) (official visualizer)\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share\" allowfullscreen></iframe>"
// }

// describe("Tests for about card", () => {
//     test('myCard defined test', () => {
//         const { getByTestId, queryByTestId } = render (
//             <BrowserRouter>
//                 <MyCard title={mockData.name} text={mockData.bio} {...mockData} link={("/Artists/" + mockData.id)} />;
//             </BrowserRouter>
//             );
//             expect(queryByTestId('cardTest')).toBeDefined;
            
        
//     });
//     test('myCard name test', () => {
//         const { getByTestId, queryByTestId } = render (
//             <BrowserRouter>
//                 <MyCard title={mockData.name} text={mockData.bio} {...mockData} link={("/Artists/" + mockData.id)} />;
//             </BrowserRouter>
//             );
//             expect(screen.getByTestId('testTitle').closest('div')).toHaveTextContent('mazie');
        
//     });
//     // test('myCard no name test', () => {
//     //     const test = render (
//     //     <BrowserRouter>
//     //         <MyCard />;
//     //     </BrowserRouter>
//     //     )
//     //     expect(test.getByTestId('testTitle').closest('div')).toHaveTextContent('');
//     // });
//     test('myCard image test', () => {
//         const { getByTestId, queryByTestId } = render (
//             <BrowserRouter>
//                 <MyCard title={mockData.name} foolowers= {99} text={mockData.bio} {...mockData} link={("/Artists/" + mockData.id)} />;
//             </BrowserRouter>
//             );
//             expect(screen.getByTestId('imageTest').closest('img')).toHaveAttribute('src', 'https://www.wearetheguard.com/sites/default/files/styles/main_blog_full_image_style/public/best-new-artists-mazie.jpg?itok=yftJuk0T');
        
//     });
//     test('myCard no image test', () => {
//         const { getByTestId, queryByTestId } = render (
//             <BrowserRouter>
//                 <MyCard />;
//             </BrowserRouter>
//             );
//             expect(screen.getByTestId('imageTest').closest('img')).toHaveTextContent('');
        
//     });
    
// });