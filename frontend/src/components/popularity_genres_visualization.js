import NavBar from '../components/NavBar';
import Header from '../components/Header.js';
import Background from '../static/images/home-banner.png';
import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';


export default function Popularity_genres_visualizations() {
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    const [tempPosts, setTempPosts] = useState([]);
    useEffect(() => {
        // console.log("inside use effect")
        const fetchPosts = async () => {
            if (!loaded) {
                var query = `https://api.lowkeys.me/genres`;
                const res = await axios.get(query);
                // setTempPosts(res.data.genres);
                var temp = res.data.genres;
                console.log("temp")
                console.log(temp[0]["id"])
                
                const count = res.data.meta.count;
                console.log(count)
                var tempArray = [];
                for (var i = 0; i < count; i++) {
                    var tempDict = {};
                    tempDict["Genre"] = temp[i]["id"]; 
                    tempDict["Popularity"] = temp[i]["popularity"];
                    tempArray.push(tempDict);
                }
                console.log(tempArray)
                setPosts(tempArray);
                //setTimeout(()=>console.log(posts), 1000);
                setLoaded(true);
                
            }
        }
        fetchPosts();
    }, [loaded, tempPosts, posts]);

    useEffect(() => {
        console.log(posts)
    }, [posts])



    return (
      
        <div className="App">
          <Header title = "Our Visualizations" subtitle = "Analyzing our data" url={Background} /> 
          <h1 className="header-one">Explore our visualizations:</h1>
          <div style={{display:'flex', flexDirection:'row', justifyContent: 'space-evenly', padding: '10px'}}>
          </div>
      
        </div>
      
    );
  }