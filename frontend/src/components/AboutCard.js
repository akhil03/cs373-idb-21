import React from 'react'

// function Card() {
//   return (
//     <div>Card</div>
//   )
// }

// export default Card;
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button';
import './APICard.css';

export default function AboutCard (props) {
  return (
    <>
      <Card className='card' data-testid='cardTest' onClick={() => {console.log('clicked!')}}>
        <Card.Body style={{padding: '0px'}}>
          <Card.Img variant="top" src={props['image']} style={{ border: '1px solid darkgray', width: '100%', height:'55%'}}/>
          <div style={{ paddingTop: '5px', borderTop: '1px solid black', borderBottom: '1px solid black', backgroundColor: 'darkgray', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <Card.Title data-testid= 'title' style={{ padding: '10px', color: "#36783d",  textAlign: 'center'}}> {props['name']} </Card.Title>
          </div>
          <Card.Text style={{padding:'10px', margin:'0px', backgroundColor: 'white'}}>
            Role: {props['role']}<br />
            GitLab: {props['gitlab']}
          </Card.Text>
          <Card.Text data-testid= 'details' style={{padding:'10px', margin:'0px', backgroundColor: 'lightGray'}}>
            {props['bio']}
          </Card.Text>
          <Card.Text style={{ padding:'10px', marginTop:'-7px'}}>
            Commits: {props['commits']}<br />
            Issues: {props['issues']}<br />
            Unit Tests: {props['unittests']}
          </Card.Text>

          {/*<Card.Link style={{padding:'20px'}} href={props['link']}>{props['linkText']}</Card.Link> */}
        </Card.Body>
      </Card>
    </>
  );
}