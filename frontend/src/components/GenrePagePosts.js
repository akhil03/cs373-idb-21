import React, { useState, useEffect } from 'react';
import MyCard from './MyCard';
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import axios from 'axios';


const GenrePagePosts = ({posts, loaded, query}) => {
    
    //setTimeout (() => set_genre_popularities, 500);

    //useEffect (() => {}, [dummy]);
    
    
    return (
        <div className="list-group mb-4">  
            {loaded ? (
                <Row style={{width: '100%'}}
                    xs={1}
                    sm={2}
                    md={3}
                    xl={5}
                    className="g-4 p-4 justify-content-center"
                    >
                    {posts.map((post) => {
                        // const arr = props['artists'];
                        
                        return (
                            <Col className="d-flex flex-grow-1 justify-content-center">
                                <MyCard
                                    id = {post.id}
                                    image={post.image}
                                    title={post.id}
                                    link={("/Genres/" + post.id)}
                                    artists={post.artist_num}
                                    artists_names={post.artists_names.join(', ')}
                                    songs={post.song_num}
                                    popularity= {post.popularity}
                                    followers={post.followers}
                                    query={query}
                                />
                            </Col>
                        );
                    })}
                </Row>
                ) : (
                <Row style={{width: '100%'}}>
                    <Col className="d-flex justify-content-center">
                    <Spinner animation="grow" />
                    </Col>
                </Row>
            )}
        </div>
    );
};

export default GenrePagePosts