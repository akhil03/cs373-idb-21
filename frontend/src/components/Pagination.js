import React from 'react'
import './Pagination.css'
const Pagination = ({postsPerPage, totalPosts, paginate, currentPage, nextPage, prevPage, maxPages}) => {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }
    
    return (

        <nav style={{backgroundColor: 'lightgray', display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
            <div className="pagination">
                {currentPage > 1 ? <button style={{backgroundColor: 'white', color: 'blue'}} onClick={() => prevPage(currentPage)} href="#!">{String.fromCharCode(8592)}</button>:""}
                {currentPage > 1 ? <button style={{backgroundColor: 'white', color: 'blue', width:'50px'}} onClick={() => paginate(1)} href="#!">FIRST</button>:""}
                {currentPage >= 3 ? <button style={{backgroundColor: 'white', color: 'blue'}} onClick={() => paginate(currentPage - 2)} href="#!">{[currentPage-2]}</button>:""}
                {currentPage >= 2 ? <button style={{backgroundColor: 'white', color: 'blue'}} onClick={() => paginate(currentPage - 1)} href="#!">{[currentPage-1]}</button>:""}
                    <button  onClick={() => paginate(currentPage)} href="#!" className={"active"}>
                            {currentPage}
                    </button>
                {currentPage + 1 <= maxPages ? <button style={{backgroundColor: 'white', color: 'blue'}} onClick={() => paginate(currentPage + 1)} href="#!">{[currentPage+1]}</button>:""}
                {currentPage + 2 <= maxPages ? <button style={{backgroundColor: 'white', color: 'blue'}} onClick={() => paginate(currentPage + 2)} href="#!">{[currentPage+2]}</button>:""}
                
                
               

                {currentPage < maxPages ? <button style={{backgroundColor: 'white', color: 'blue', width: '45px'}} onClick={() => paginate(maxPages)} href="#!">LAST</button>:""}
                {currentPage < pageNumbers[pageNumbers.length-1] ? <button style={{backgroundColor: 'white', color: 'blue'}} onClick={() => nextPage(currentPage)} href="#!">{String.fromCharCode(8594)}</button>:""}
            </div>
            

        </nav>
    )
}

export default Pagination