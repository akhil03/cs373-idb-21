import React, { useState } from "react";

import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Container from "react-bootstrap/Container";

const FilterDropdown = (props) => {
  let { title, items, scroll, onChange } = props;
  if (title === undefined) {
    title = 'default';
  }
  if (items === undefined) {
    items = ['default'];
  }
  if (onChange === undefined) {
    onChange = (value) => {console.log(value)};
  }

  const [choice, setChoice] = useState(title);

  const handleClick = (value) => {
    setChoice(value);
    onChange(value);
  };

  return (
    <div style={{marginBottom: '20px', paddingBottom: '20px'}}>
    <DropdownButton title={choice} style={{position: 'absolute', paddingBottom: '10px' , justifyContent: 'center', marginLeft: '128px'}}>
      <div style={{alignContent: 'center', justifyContent: 'center'}}>
        <Dropdown.Menu style={{maxHeight: "500px" ,overflowX: 'auto', overflowY: "scroll", right: 0, position: 'absolute'}} >

        {items.map((item) => {
            return (
                <Dropdown.Item key={item} onClick={() => handleClick(item)}>
                {item}
                </Dropdown.Item>
            );
          })}

      </Dropdown.Menu>
      </div>
    {/* </Container> */}
    </DropdownButton>
    {/* <div style={{marginBottom: '20px', paddingBottom: '20px'}}></div> */}
    </div>
  );
};

export default FilterDropdown;
