import React from 'react'

// function Card() {
//   return (
//     <div>Card</div>
//   )
// }

// export default Card;
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button';
import './AboutCard.css';
import { useNavigate } from "react-router-dom";
import Highlighter from 'react-highlight-words';
const styles = {
  cardImage: {
    objectFit: 'cover',
    border: '1px solid darkgrey',
    width: '100%',
    height: '150px',
    borderRadius: '5px'
    
  },
}
  
export default function ArtistCard (props) {
  //props: link, image, title, text, linkText
  function highlightText(input) {
    if (props.query != null) {
      // console.log("Regex is below")
      let decoded_query = decodeURI(props.query);
      // console.log(props.query)
      // console.log(decoded_query.split(' ').flat())
      return <Highlighter 
        highlightClassName='highlight'
        //The%20Summer%20Set
        searchWords={decoded_query.split(' ')}
        autoEscape={false}
        textToHighlight={input}
      />;
    }
    return input;
  }
  
  const navigate = useNavigate();

  //Comment back in when we have actual API
  // let trunc_text = props.text;
  // if (props.full_text) {
  // } else {
  //   trunc_text = props.text.replace(/(.{100})..+/, "$1…");
  // }
  
  return (
    <>
      <Card data-testid = 'cardTest' className='card' onClick={() => {navigate(props['link'])}}>
        <Card.Body style={{padding: '0px', width: '100%'}}>
          <Card.Img data-testid='imageTest' variant="top" src={props['image']} alt={'Artist has no image'} style={styles.cardImage}/>
          <div style={{ paddingTop: '5px', borderTop: '1px solid black', borderBottom: '1px solid black', width: '100%', height: '60px', backgroundColor: 'darkgray', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <Card.Title data-testid='testTitle' style={{ padding: '10px', color: "#36783d",  textAlign: 'center'}}> {highlightText(String(props['title']))} </Card.Title>
          </div>
          <Card.Text style={{padding:'10px'}}>
            <span style={{ fontWeight: 'bold' }}>Genres:</span> {
              // highlightText(String([props['genres']].flat().join(', ')))
              highlightText(String(props['genres']) ? [props['genres']].flat().join(', ') : 'N/A')
            }<br/>
            <span style={{ fontWeight: 'bold' }}>Number of Albums:</span> {props['albums']}<br/>
            <span style={{ fontWeight: 'bold' }}>Number of Singles:</span> {props['singles']}<br/>
            <span style={{ fontWeight: 'bold' }}>Artist Popularity:</span> {props['popularity']} / 100<br/>
            <span style={{ fontWeight: 'bold' }}>Number of Followers:</span> {props['followers'].toLocaleString("en-US")}
          </Card.Text>
          {/* <Card.Text style={{padding:'20px'}}>
            {trunc_text}
          </Card.Text> */}

          {/* <Card.Link style={{padding:'20px'}} href={props['link']}>{props['linkText']}</Card.Link> */}
        </Card.Body>
      </Card>
    </>
  );
}


