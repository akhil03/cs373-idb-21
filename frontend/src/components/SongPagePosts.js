import React, { useState, useEffect } from 'react';
import SongCard from './SongCard.js'
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";


const SongPagePosts = ({posts, loaded, query, currentPosts, setLoaded}) => {
    // const [thePosts, setPosts] = useState([]);
    // const [load, setLoad] = useState(false);
    // if (loading) {
    //     return <h2>Loading...</h2>
    // }
    // console.log("currentPosts")
    // console.log(currentPosts)
    // useEffect(() =>{
    //     const fetchPosts = async () => {
    //         setLoad(false)
    //         if (currentPosts != null) {
    //             console.log("Am i in here?")
    //             setPosts(currentPosts)
    //         } else {
    //             setPosts(posts)
    //         }
        
    //     }
    //     console.log(thePosts)
    //     setLoad(true)
    //     fetchPosts();
    // },[]);
    
    // console.log("currentPosts NEW")
    
    
    
    return (
        
        <div className="list-group mb-4">  
            {loaded ? (
                <Row
                    xs={1} //1
                    sm={2} //2
                    md={3} //3
                    lg={5} //5
                    xl={5} //5
                    className="d-flex g-4 p-4 justify-content-center "
                >
                    
                    {/* <div style={{width: '100%'}}>  */}
                        {posts.map((post) => {
                            return (
                                <Col className="d-flex flex-grow-1 justify-content-center align-self-stretch" >
                                    <SongCard
                                        artists_names={post.artists_names} // used to be artistsDecoded
                                        image={post.images}
                                        title={post.name}
                                        duration={post.duration}
                                        popularity={post.popularity + ' / 100'}
                                        genres={post.genres}
                                        link={"/Songs/" + post.id}
                                        query={query}
                                    />
                                </Col>
                            );
                        })}
                    {/* </div> */}
                    
                </Row>
                ) : (
                <Row>
                    <Col className="d-flex justify-content-center">
                    <Spinner animation="grow" />
                    </Col>
                </Row>
            )}
        </div>
    );
};

export default SongPagePosts