import os
import unittest
from sys import platform
import argparse

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

url = "https://www.lowkeys.me/"

#for reference
if __name__ == "__main__":
    if platform == "win32":
        PATH = "./guitests/chromedriver_win32.exe"
    elif platform == "linux":
        PATH = "./guitests/chromedriver_linux"
    elif platform == "darwin":
        PATH = "./guitests/chromedriver_darwin"
    else:
        print("Unsupported OS")
        exit(-1)
    
    print("platform:", platform)
    
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument(PATH, help="Path to the chromedriver executable")
    args = parser.parse_args()

    chromedriver_path = args.chromedriver_path 
    '''
   
    os.system("echo Running Page Tests")
    #command = f"python3 ./guitests/page_tests.py {PATH}"
    #os.system(command)
    os.system("python3 ./guitests/page_tests.py ")

    #os.system("echo Running Title Tests")
    #os.system("python3 ./guitests/title_tests.py ")

    #os.system("echo Running Navbar Tests")
    #os.system("python3 ./guitests/nav_tests.py ")

    os.system("echo Running Filter Tests")
    os.system("python3 ./guitests/filter_tests.py ")
